

const errorOverlayMiddleware = require("react-dev-utils/errorOverlayMiddleware");
const noopServiceWorkerMiddleware = require("react-dev-utils/noopServiceWorkerMiddleware");
const ignoredFiles = require("react-dev-utils/ignoredFiles");
const config = require("./webpack.config.dev");
const paths = require("./paths");

const protocol = process.env.HTTPS === "true" ? "https" : "http";
const host = process.env.HOST || "0.0.0.0";

module.exports = function(proxy, allowedHost) {
  return {
    disableHostCheck:
      !proxy || process.env.DANGEROUSLY_DISABLE_HOST_CHECK === "true",
    compress: true,

    clientLogLevel: "none",

    contentBase: paths.appPublic,
    watchContentBase: true,

    hot: true,

    publicPath: config.output.publicPath,

    quiet: true,

    watchOptions: {
      ignored: ignoredFiles(paths.appSrc)
    },
    https: protocol === "https",
    host: host,
    overlay: false,
    historyApiFallback: {
      disableDotRule: true
    },
    public: allowedHost,
    proxy: {
      "/api/": {
        target: "https://yyd.younidin.com",
        // target: "http://192.168.31.100:8006/",
        pathRewrite: { "^/api/": "/api/v1/" },
        secure: true,
        changeOrigin: true
      }
    },
    before(app) {
      app.use(errorOverlayMiddleware());

      app.use(noopServiceWorkerMiddleware());
    }
  };
};
