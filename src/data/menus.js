export const adminMenus = [
  {
    key: "/app/shop",
    title: "服务商管理",
    icon: "edit",
    sub: [
      { key: "/app/shop/view", title: "查看服务商", icon: "" },
      { key: "/app/shop/user", title: "代理管理", icon: "" },
      { key: "/app/category", title: "分类管理", icon: "" }
    ]
  },
  {
    key: "/app/order",
    title: "订单管理",
    icon: "tool",
    sub: [{ key: "/app/order/view", title: "查看订单", icon: "" }]
  },
  {
    key: "/app/report",
    title: "报表管理",
    icon: "bar-chart",
    sub: [{ key: "/app/report/view", title: "查看报表", icon: "" }]
  },
  { key: "/app/setting", title: "基本设置", icon: "home" },
  { key: "/app/pwd", title: "修改密码", icon: "lock" },
  { key: "/app/phone", title: "修改手机号", icon: "lock" },
  {
    key: "/app/carousel",
    title: "轮播图管理",
    icon: "picture",
    sub: [
      { key: "/app/carousel/view", title: "查看轮播图", icon: "" },
      { key: "/app/carousel/add", title: "添加轮播图", icon: "" }
    ]
  },
  { key: "/app/qs", title: "用户反馈", icon: "question" },

];
export const proxyMenus = [
  {
    key: "/app/shop",
    title: "服务商管理",
    icon: "edit",
    sub: [{ key: "/app/shop/view", title: "查看服务商", icon: "" }]
  },
  {
    key: "/app/order",
    title: "订单管理",
    icon: "tool",
    sub: [{ key: "/app/order/view", title: "查看订单", icon: "" }]
  },
  {
    key: "/app/report",
    title: "报表管理",
    icon: "bar-chart",
    sub: [{ key: "/app/report/view", title: "查看报表", icon: "" }]
  },
  { key: "/app/pwd", title: "修改密码", icon: "lock" },
  { key: "/app/phone", title: "修改手机号", icon: "lock" },
];
