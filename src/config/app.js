import React, { Component } from "react";
import { Layout, Menu, Icon, Dropdown, message } from "antd";
// import { CSSTransition } from 'react-transition-group';
import AdminRouter from "../routes";
import HeaderMenu from "../page/sideMenu/headerMenu";
import { getToken } from "../axios/tools";
import cookie from "js-cookie";

import "../style/css/app.css";

const { Header, Sider, Content } = Layout;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      SiderMarginLeft: "200px",
      menu: (
        <Menu>
          {/* <Menu.Item onClick={this.chpwd} key="1">修改密码</Menu.Item> */}
          <Menu.Item onClick={this.logout} key="2">
            退出登录
          </Menu.Item>
        </Menu>
      )
    };
  }
  componentWillMount = () => {
    const Token = getToken();
    if (Token === null) {
      // message.success('登录凭证失效，正在跳转登录界面...',0.5,()=>this.props.history.push('/login'));
    }
  };

  toggle = () => {
    const ClientMarginLeft = this.state.collapsed ? "200px" : "80px";
    this.setState({
      SiderMarginLeft: ClientMarginLeft,
      collapsed: !this.state.collapsed
    });
  };

  logout = () => {
    message.success(
      "注销成功，正在跳转登录界面...",
      0.5,
      () => {
        document.location.replace("/login");
      }
      // this.props.history.replace("/login")
    );
  };

  chpwd = () => {
    this.props.history.push("/app/setting/chpwd");
  };
  render() {
    return (
      <Layout className="admin-layout">
        <Sider
          className="admin-sider"
          onCollapse={this.toggle}
          collapsible
          collapsed={this.state.collapsed}
        >
          <div
            className="admin-logo"
            style={{ visibility: this.state.collapsed ? "hidden" : "visible" }}
          >
            <h1 className={this.state.collapsed ? "" : "animated bounceInLeft"}>
              优宜定后台管理系统
            </h1>
          </div>
          <HeaderMenu />
        </Sider>
        <Layout style={{ marginLeft: this.state.SiderMarginLeft }}>
          <Header className="admin-header">
            {/* <a className="ant-dropdown-link" target="_blank"  href="/">
              前台首页
            </a> */}
            <Dropdown overlay={this.state.menu}>
              <a className="ant-dropdown-link">
                {cookie.get("_U")}
                <Icon type="down" />
              </a>
            </Dropdown>
          </Header>
          <Content className="admin-content">
            <AdminRouter />
          </Content>
          {/* <Footer className="admin-footer" >
          省级动画与数字技术实验教学示范中心 © <a rel="noopener noreferrer" href="http://nmc.zjicm.edu.cn">浙江传媒学院新媒体学院</a> 2010-{moment().format('YYYY')}
          </Footer> */}
        </Layout>
      </Layout>
    );
  }
}

export default App;
