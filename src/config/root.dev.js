import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom'
import DevTools from './devTools'
import Loadable from 'react-loadable';
import Loading from '../components/loading/loading';

const App = Loadable({
    loader: () => import('./app'),
    loading: Loading,
});

const NotFound = Loadable({
    loader: () => import('../page/404/'),
    loading: Loading,
});

const Login = Loadable({
    loader: () => import('../page/login/login'),
    loading: Loading,
});


 const Root = ({ store }) => (
    <Switch>
        <Route exact path="/" 
        render={() => (
            <Redirect to="/login"/>
        )}
        />
        <Route path="/app" component={App} />
        <Route path="/login" component={Login} />
        <Route path="/404" component={NotFound} />
        <Route path="/loading" component={Loading} />
        <Route component={NotFound} />
        <DevTools />
    </Switch>
)



export default Root;