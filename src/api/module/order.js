import '../../axios/index'
import axios from 'axios';

//订单，分页搜索
export const ordersearch = data =>
  axios.get('order/search', {
    params: data
  })
    .then(res => Promise.resolve(res.data));   

//批量转账
export const ordertrans = data =>
    axios.put('order/trans', data)
        .then(res => Promise.resolve(res.data));    