import "../../axios/index";
import axios from "axios";

//设置，修改
export const setupupdate = data =>
  axios.put("setup/update", data).then(res => Promise.resolve(res.data));

//设置，根据id
export const setupid = data =>
  axios
    .get("setup/id", {
      params: data
    })
    .then(res => Promise.resolve(res.data));

//协议，修改
export const protocolpdate = data =>
  axios.put("protocol/update", data).then(res => Promise.resolve(res.data));

//协议，获得
export const protocolid = data =>
  axios
    .get("protocol/id", {
      params: data
    })
    .then(res => Promise.resolve(res.data));

//管理员登陆
export const adminlogin = data =>
  axios.post("admin/login", data).then(res => Promise.resolve(res.data));

//管理员，根据id
export const adminid = data =>
  axios
    .get("admin/id", {
      params: data
    })
    .then(res => Promise.resolve(res.data));

//管理员，修改
export const adminupdate = data =>
  axios.put("admin/update", data).then(res => Promise.resolve(res.data));

//轮播图，获得
export const carouselsearch = data =>
  axios
    .get("carousel/search", {
      params: data
    })
    .then(res => Promise.resolve(res.data));
//轮播图，修改
export const carouselupdate = data =>
  axios.put("carousel/update", data).then(res => Promise.resolve(res.data));

//轮播图，删除
export const carouseldelete = data =>
  axios
    .delete("carousel/delete/" + data)
    .then(res => Promise.resolve(res.data));

//轮播图，创建
export const carouselcreate = data =>
  axios
    .post("carousel/create" , data)
    .then(res => Promise.resolve(res.data));    

    