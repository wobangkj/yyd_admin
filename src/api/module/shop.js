import '../../axios/index'
import axios from 'axios';

//服务商，分页搜索
export const shopsearch = data =>
  axios.get('shop/search', {
    params: data
  })
    .then(res => Promise.resolve(res.data));

//服务商，修改
export const shopupdate = data =>
    axios.put('shop/update', data)
        .then(res => Promise.resolve(res.data));        

//服务商，删除
export const shopdelete = data =>
axios.delete('shop/delete/'+data)
    .then(res => Promise.resolve(res.data));      