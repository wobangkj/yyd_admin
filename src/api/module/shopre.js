import '../../axios/index'
import axios from 'axios';

//账单，分页搜索
export const shopresearch = data =>
  axios.get('shopre/search', {
    params: data
  })
    .then(res => Promise.resolve(res.data));     
//转账记录
export const shoprecreate = data =>
    axios.post('shopre/create', data)
        .then(res => Promise.resolve(res.data)); 