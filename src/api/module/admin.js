import "../../axios/index";
import axios from "axios";

/**
 *
 * 管理员，修改
 *
 */
export const adminupdate = data =>
  axios.put("/admin/update", data).then(res => Promise.resolve(res.data));

export const admincreate = data =>
  axios.post("/admin/create", data).then(res => Promise.resolve(res.data));

export const admindelete = data =>
  axios.delete("/admin/delete/" + data).then(res => Promise.resolve(res.data));

export const adminsearch = data =>
  axios.get("/admin/search", data).then(res => Promise.resolve(res.data));
export const getCode = data =>
  axios.post("/sms/sendsms", data).then(res => Promise.resolve(res.data));
export const getPhone = data =>
  axios.get("/admin/getp", {
    params: data
  }).then(res => Promise.resolve(res.data));
export const setPhone = data =>
  axios.post("/admin/setp", data).then(res => Promise.resolve(res.data));
export const editp = data =>
  axios.post("/admin/editp", data).then(res => Promise.resolve(res.data));