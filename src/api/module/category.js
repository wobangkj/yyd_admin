import '../../axios/index'
import axios from 'axios';

//分类，分页搜索(树选择与parent_id进行二级下拉框渲染)
export const categorysearchx = data =>
    axios.get('category/searchx', {
        params: data
    })
        .then(res => Promise.resolve(res.data));

//分类，创建
export const categorycreate = data =>
    axios.post('category/create', data)
        .then(res => Promise.resolve(res.data));     

//分类，修改
export const categoryupdate = data =>
    axios.put('category/update', data)
        .then(res => Promise.resolve(res.data));             

//分类，删除
export const categorydelete = data =>
axios.delete('category/delete/'+data)
    .then(res => Promise.resolve(res.data));             