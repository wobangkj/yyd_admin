import '../../axios/index'
import axios from 'axios';

//建议，分页，修改
export const suggestsearch = data =>
    axios.get('suggest/search', {
        params: data
    })
        .then(res => Promise.resolve(res.data));