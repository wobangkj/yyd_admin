import '../../axios/index'
import axios from 'axios';

/**
 * 
 * 获取文件上传必要信息
 * 
 */
export const getossinfo = data => 
    axios.get('/oss/getossinfo',{
      params: data
    })
    .then( res => Promise.resolve(res.data));

/**
 * 
 * 批量新增试题
 * 
 */
export const createsomething = data => 
    axios.post('/file/createsomething',data, {
      headers: { 'Content-Type': 'multipart/form-data' }
  })
    .then( res => Promise.resolve(res.data));    