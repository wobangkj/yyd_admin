import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './config/registerServiceWorker';
import Root from './config/root';
import { HashRouter as Router } from 'react-router-dom'
import { AppContainer } from 'react-hot-loader'
import { LocaleProvider } from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import 'moment/locale/zh-cn';


const render = Component => {
    ReactDOM.render(
        <LocaleProvider locale={zh_CN}>
            <AppContainer>
                <Router>
                    <Component />
                </Router>
            </AppContainer>
        </LocaleProvider>,
        document.getElementById('root')
    );
};

render(Root);

if (module.hot) {
    // 详情可参照https://github.com/gaearon/react-hot-loader/issues/298
    const orgError = console.error; // eslint-disable-line no-console
    console.error = (...args) => { // eslint-disable-line no-console
        if (args && args.length === 1 && typeof args[0] === 'string' && args[0].indexOf('You cannot change <Router routes>;') > -1) {
            // React route changed
        } else {
            // Log the error as normally
            orgError.apply(console, args);
        }
    };
    module.hot.accept('./config/root', () => {
        render(Root);
    })
}
registerServiceWorker();
