import { message, Modal } from 'antd';
import * as C from './config';
import axios from 'axios';
// import { getToken } from './tools';
import qs from 'qs'
import np from 'nprogress';
import { isArray } from 'util';
const confirm = Modal.confirm;

const source = axios.CancelToken.source();

axios.defaults.baseURL = C.API_ROOT;
// axios.defaults.timeout = 5000;
axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
axios.interceptors.request.use(config => {
  const ignoreRoute = [
    '/file/createsomething'
  ]
  np.start();
  config.withCredentials = true;
  if(!(ignoreRoute.includes(config.url)) && config.method !== 'get') {
    config.data = qs.stringify(config.data);
    console.log(config.data)
  }
  if(config.method === 'delete'){
    return new Promise((resolve,reject)=>{
      confirm({
        title: '警告',
        content: '您确认删除它吗?',
        cancelText:'取消',
        okText:'确认删除',
        onOk() {
          resolve(config);
        },
        onCancel() {
          source.cancel(211);
          reject();
        },
      });
    })
  }
  return config;
}, error => {
  // console.error(error)
  message.error("网络请求失败，请确认网络已接入服务商！")
  np.done();
  Promise.reject(error)
})

axios.interceptors.response.use(
  response => {
    np.done();
    if (response.data.status.toString() === '201') {
      message.success("创建成功！")
      return Promise.resolve(response);
    }
    else if (response.data.status.toString() === '202') {
      message.warn("修改成功！")
      return Promise.resolve(response);
    }
    else if (response.data.status.toString() === '203') {
      message.warn("账号或密码错误！")
      return Promise.reject(response);
    }
    else if (response.data.status.toString() === '204') {
      message.warn("暂无数据！")
      // window.history.go(-1);
      return Promise.reject(response);
    }
    else if (response.data.status.toString() === '206') {
      message.success("修改成功！")
      return Promise.resolve(response);
    }
    else if (response.data.status.toString() === '209') {
      message.success("删除成功！")
      return Promise.resolve(response);
    }
    else if (response.data.status.toString() === '210') {
      const errorResult = response.data;
      if(isArray(errorResult.data)){
        for(var i = 0, len = errorResult.data.length; i < len; i++){
          message.warn(errorResult.data[i])
        }
      } else {
        message.warn(errorResult.msg)
      }
      return Promise.reject(response);
    }
    else if (response.data.status.toString() === '211') {
      message.warn("用户名或密码错误！")
      return Promise.reject(response);
    }
    else if (response.data.status.toString() === '222') {
      message.warn("内部错误！")
      return Promise.reject(response);
    }
    else if (response.data.status.toString() === '271') {
      message.warn(response.data.msg)
      return Promise.reject(response);
    }
    else if (response.data.data && response.data.data.length === 0) {
      message.warn("暂无数据！")
      return Promise.reject(response);
    }
    else {
      return response;
    }
  },
  error => {
    // console.log('err', error === undefined)
  if(error !== undefined){
    console.error("系统繁忙！")
  }
  np.done();
  return Promise.reject(error)
  }
)

