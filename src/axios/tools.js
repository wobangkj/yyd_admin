import * as C from 'js-cookie';

/**
 * @description 返回token
 */
export const getToken = () => {
  if (C.get('_TOKEN') === sessionStorage.getItem("_TOKEN")){
    return sessionStorage.getItem("_TOKEN");
  } else {
    return null;
  }
}

/**
 * @description 设置token
 * @param string _TOKEN
 */
export const setToken = _TOKEN => {
  sessionStorage.setItem('_TOKEN', _TOKEN);
}