import React, { Component } from "react";
import { adminMenus, proxyMenus } from "../../data/menus";
import SiderMenu from "./siderMenu";
import { hot } from "react-hot-loader";

class HeaderCustom extends Component {
  render() {
    return (
      <SiderMenu
        menus={sessionStorage.getItem("role") == 1 ? adminMenus : proxyMenus}
        theme="dark"
        mode="inline"
        defaultSelectedKeys={["home"]}
      />
    );
  }
}

export default hot(module)(HeaderCustom);
