import React from 'react';
import { Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';
import { hot } from 'react-hot-loader'

// import { CSSTransition } from 'react-transition-group';

const SubMenu = Menu.SubMenu;
// import * as _ from 'lodash';

const renderMenuItem = ({key, title, icon, link, sub}) => 
    <Menu.Item key={key} >
        <Link to={key}>
            {icon && <Icon type={icon} />}
            <span>{title}</span>
        </Link>
    </Menu.Item>;

const renderSubMenu = ({key, title, icon, link, sub}) => 
    <SubMenu
            key={key}
            title={
                <span>
                    {icon && <Icon type={icon} />}
                    <span>{title}</span>
                </span>
            }
        >
            {sub && sub.map(item => renderMenuItem(item))}
        </SubMenu>


export default  hot(module)(({ menus, ...props}) => <Menu {...props}>
    {menus && menus.map(
    item => item.sub && item.sub.length ? 
        renderSubMenu(item) : renderMenuItem(item)
    )}
</Menu>);
