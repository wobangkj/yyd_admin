import React, { Component } from "react";
import {
  Tabs,
  Input,
  Button,
  Form,
  Icon,
  Upload,
  Modal,
  message,
  Select
} from "antd";
import { hot } from "react-hot-loader";
import { setupupdate, setupid, protocolpdate, protocolid } from "../../api";
import { UPLOAD_IMG, API_ROOT_FILE } from "../../axios/config";

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const { Option } = Select;
class ColumnAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formItemLayout: {
        labelCol: {
          xs: { span: 12 },
          sm: { span: 4 }
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 12 }
        }
      },
      tailFormItemLayout: {
        wrapperCol: {
          xs: {
            span: 12,
            offset: 4
          },
          sm: {
            span: 12,
            offset: 4
          }
        }
      },
      residencesCategory: [],
      previewVisible: false,
      previewImage: "",
      fileList: []
    };
  }

  componentWillMount() {
    protocolid({
      id: 1
    }).then(res => {
      this.setState({
        shopprotocol: res.data.protocol
      });
    });

    protocolid({
      id: 2
    }).then(res => {
      this.setState({
        userprotocol: res.data.protocol
      });
    });

    setupid({
      id: 1
    }).then(res => {
      this.setState({
        phone: res.data.value
      });
    });
    setupid({
      id: 3
    }).then(res => {
      const value = res.data.value.split(",");
      this.setState({
        code0: value[0],
        code1: value[1],
        code2: value[2],
        code3: value[2]
      });
    });
    setupid({
      id: 4
    }).then(res => {
      const value = res.data.value.split(",");
      this.setState({
        isVipPrice: value[0],
        vipPrice: value[1]
      });
    });

    setupid({
      id: 2
    }).then(res => {
      this.setState({
        fileList:
          res.data.value &&
          res.data.value.split(",").map((item, index) => ({
            url: API_ROOT_FILE + item,
            response: {
              filename: item
            },
            uid: index
          }))
      });
    });
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        setupupdate({
          id: 1,
          value: values.phone
        });
        setupupdate({
          id: 3,
          value: [values.code0, values.code1, values.code2, values.code3].join(
            ","
          )
        });
        setupupdate({
          id: 4,
          value: [values.isVipPrice, values.vipPrice].join(",")
        });
        // setupupdate({
        //     id:2,
        //     value: this.state.fileList.map(item=>item.response.filename).join(',')
        // })
        protocolpdate({
          id: 1,
          protocol: values.shopprotocol
        });
        protocolpdate({
          id: 2,
          protocol: values.userprotocol
        });
      }
    });
  };
  handleReset = () => {
    this.props.form.resetFields();
  };

  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = file => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true
    });
  };

  beforeUpload(file) {
    // const isJPG = file.type === 'image/jpeg';
    // if (!isJPG) {
    //   message.error('You can only upload JPG file!');
    // }
    const isLt2M = file.size / 1024 / 1024 < 1;
    if (!isLt2M) {
      message.error("图片最大不能超过1MB!");
    }
    // return isJPG && isLt2M;
    return isLt2M;
  }
  handleChange = ({ fileList }) => this.setState({ fileList });

  render() {
    const { getFieldDecorator } = this.props.form;
    const { previewVisible, previewImage, fileList } = this.state;
    console.log(fileList);

    const fileLists = [];
    fileList &&
      fileList.filter(item => {
        if (item.status === "uploading" || item.response) {
          fileLists.push(item);
        }
      });

    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">上传</div>
      </div>
    );
    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab="平台管理" key="1">
          <Form onSubmit={this.handleSubmit}>
            <FormItem {...this.state.formItemLayout} label="商家申请协议">
              {getFieldDecorator("shopprotocol", {
                rules: [{ required: true, message: "请输入商家申请协议!" }],
                initialValue: this.state.shopprotocol
              })(<Input.TextArea rows={12} />)}
            </FormItem>
            <FormItem {...this.state.formItemLayout} label="用户注册协议">
              {getFieldDecorator("userprotocol", {
                rules: [{ required: true, message: "请输入用户注册协议!" }],
                initialValue: this.state.userprotocol
              })(<Input.TextArea rows={12} />)}
            </FormItem>
            {/* <FormItem {...this.state.formItemLayout}
                            label="平台轮播图">
                        {getFieldDecorator('fileList', {
                         })(    
                          <div className="clearfix">
                            <Upload
                              action={UPLOAD_IMG}
                              listType="picture-card"
                              fileList={fileLists}
                              accept="image/gif,image/jpeg,image/jpg,image/png,image/svg"
                              beforeUpload={this.beforeUpload}
                              onPreview={this.handlePreview}
                              onChange={this.handleChange}
                              data={{width:476}}
                            >
                              {fileLists.length >= 5 ? null : uploadButton}
                            </Upload>
                            <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                              <img alt="预览图" style={{ width: '100%' }} src={previewImage} />
                            </Modal>
                          </div>
                        )}
                    </FormItem> */}
            <FormItem {...this.state.formItemLayout} label="平台联系电话">
              {getFieldDecorator("phone", {
                rules: [{ required: true, message: "请输入平台联系电话!" }],
                initialValue: this.state.phone
              })(<Input />)}
            </FormItem>
            <FormItem {...this.state.formItemLayout} label="排队短信通知">
              {getFieldDecorator("code0", {
                rules: [{ required: true, message: "请选择是否排队短信通知!" }],
                initialValue: +this.state.code0
              })(
                <Select>
                  <Option key={0} value={0}>
                    否
                  </Option>
                  <Option key={1} value={1}>
                    是
                  </Option>
                </Select>
              )}
            </FormItem>
            <FormItem {...this.state.formItemLayout} label="预约短信通知">
              {getFieldDecorator("code1", {
                rules: [{ required: true, message: "请选择是否预约短信通知!" }],
                initialValue: +this.state.code1
              })(
                <Select>
                  <Option key={0} value={0}>
                    否
                  </Option>
                  <Option key={1} value={1}>
                    是
                  </Option>
                </Select>
              )}
            </FormItem>
            <FormItem {...this.state.formItemLayout} label="上门服务短信通知">
              {getFieldDecorator("code2", {
                rules: [
                  { required: true, message: "请选择是否上门服务短信通知!" }
                ],
                initialValue: +this.state.code2
              })(
                <Select>
                  <Option key={0} value={0}>
                    否
                  </Option>
                  <Option key={1} value={1}>
                    是
                  </Option>
                </Select>
              )}
            </FormItem>
            <FormItem {...this.state.formItemLayout} label="预约包厢短信通知">
              {getFieldDecorator("code3", {
                rules: [
                  { required: true, message: "请选择是否预约包厢短信通知!" }
                ],
                initialValue: +this.state.code3
              })(
                <Select>
                  <Option key={0} value={0}>
                    否
                  </Option>
                  <Option key={1} value={1}>
                    是
                  </Option>
                </Select>
              )}
            </FormItem>
            <FormItem {...this.state.formItemLayout} label="服务商年费">
              {getFieldDecorator("isVipPrice", {
                rules: [
                  { required: true, message: "请选择是否增加服务商年费!" }
                ],
                initialValue: +this.state.isVipPrice
              })(
                <Select>
                  <Option key={0} value={0}>
                    否
                  </Option>
                  <Option key={1} value={1}>
                    是
                  </Option>
                </Select>
              )}
            </FormItem>
            <FormItem {...this.state.formItemLayout} label="年费">
              {getFieldDecorator("vipPrice", {
                rules: [{ required: true, message: "请输入年费!" }],
                initialValue: this.state.vipPrice
              })(<Input />)}
            </FormItem>
            <FormItem {...this.state.tailFormItemLayout}>
              <Button type="primary" htmlType="submit">
                提交
              </Button>
              <Button htmlType="reset">重设</Button>
            </FormItem>
          </Form>
        </TabPane>
      </Tabs>
    );
  }
}
const WrappedNormalColumnAdd = Form.create()(ColumnAdd);

export default hot(module)(WrappedNormalColumnAdd);
