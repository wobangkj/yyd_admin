/* eslint-disable eqeqeq */
/* eslint-disable react/jsx-no-undef */
import React, { Component } from 'react';
import { Tabs, Input, Button, Form, message, Row, Col } from 'antd';
import { adminupdate,getCode,getPhone } from '../../api';

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 2 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 2,
    },
  },
}
class Chpwd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nickname: '',
      showCheck:false,//控制显示验证手机号
      checked:false,//控制获取验证码按钮点击状态
      time:60,//获取验证码按钮初始时间 
      phone:null,//用户手机号
    }
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillMount = () => {
    getPhone({
      account:sessionStorage.getItem('username')
    }).then(res=>{
      this.setState({
        phone:res.data.phone
      })
    })
    if(sessionStorage.getItem('role')==0){
      this.setState({
        showCheck:true
      })
    }
  }

  confirmPassword = (rule, values, callback) => {
    if (!values) {
      callback('请输入新密码！');
    }
    else if (values !== this.props.form.getFieldValue('password')) {
      callback('与新密码不一致！');
    } else {
      callback();
    }
  }

  strengthPassword = (rule, values, callback) => {
    if (!values) {
      callback('请输入密码！');
    }
    else {
      callback();
    }
  }

  countdown(){
    let that=this
    let time=this.state.time
    if(time!=0){
      this.setState({
        time:time-1
      })
      setTimeout(() => {
        that.countdown()
      }, 1000);
    }
    if(time==0){
      this.setState({
        checked:false,
        time:60
      })
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const req = {
          id: sessionStorage.getItem('uid'),
          account: values.account,
          password: values.password,
          phone:values.phone,
          code:values.code,
        }
        adminupdate(req).then((data) => {
          if(data.status==200){
            message.success("修改成功,正在跳转登录界面...", 0.5, () => this.props.history.push('/login'));
          }else{
            message.error(data.msg, 0.5);
          }
        })
      }
    });
  }
  getCode(){
    this.countdown()
    this.setState({
      checked:true
    })
    getCode({
      phone:this.state.phone,
      type:'password'
    }).then(res=>{
      if(res.status==212){
        message.success('验证码发送成功',0.5)
      }
    })
  }
  timeTip(){
    message.error(`请${this.state.time}后再次尝试`);
  }
  checkName(){
    const account=this.props.form.getFieldValue('account');
      console.log(account)
      if(account==sessionStorage.getItem('username')){
        this.setState({
          showCheck:true
        })
        getPhone({account:account}).then(res=>{
          this.setState({
            phone:res.data.phone
          })
        })
      }else{
        this.setState({
          showCheck:false
        })
      }
  }
  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab="修改密码" key="1">
          <Form onSubmit={this.handleSubmit}>
            {
              sessionStorage.getItem('role') == 0 ?
                <FormItem {...formItemLayout}
                  label="用户名" >
                  {getFieldDecorator('account', {
                    initialValue:sessionStorage.getItem('username'),
                    rules: [
                      { required: true, message: '请输入新用户名！!' },
                    ],
                  })(
                    <Input placeholder="请输入新用户名！" disabled={true}/>
                  )}
                </FormItem> :
                <FormItem {...formItemLayout}
                  label="用户名">
                  {getFieldDecorator('account', {
                    rules: [
                      { required: true, message: '请输入新用户名！!' },
                    ],
                  })(
                    <Input placeholder="请输入新用户名！"  onBlur={this.checkName.bind(this)}/>

                  )}
                </FormItem>
            }
            <FormItem {...formItemLayout}
              label="新密码">
              {getFieldDecorator('password', {
                rules: [
                  { required: true, validator: this.strengthPassword.bind(this) },
                ],
              })(
                <Input type="password" placeholder="请输入新密码(密码输入的格式为6-8位数字)" />
              )}
            </FormItem>
            <FormItem {...formItemLayout}
              label="确认新密码">
              {getFieldDecorator('confirmPassword', {
                rules: [
                  { required: true, validator: this.confirmPassword.bind(this) }
                ],
              })(
                <Input type="password" placeholder="请输入确认新密码" />
              )}
            </FormItem>
            {
              this.state.showCheck==true?
                <div>
                  <FormItem label="手机号" {...formItemLayout}>
                    <Row gutter={8}>
                      <Col span={6}>
                        {getFieldDecorator('phone', {
                          initialValue:this.state.phone,
                          rules: [{ required: true, message: '请输入手机号！' }],
                        })(<Input disabled/>)}
                      </Col>
                      <Col span={12}>
                        {
                          this.state.checked==true?
                          <Button onClick={this.timeTip.bind(this)}>{this.state.time}后再次获取验证码</Button>:
                          <Button onClick={this.getCode.bind(this)}>获取验证码</Button>
                        }
                      </Col>
                    </Row>
                  </FormItem>
                  <FormItem {...formItemLayout}
                    label="验证码">
                    {getFieldDecorator('code', {
                      rules: [
                        { required: true, message: '验证码不能为空！' }
                      ],
                    })(
                      <Input type="text" placeholder="请输入验证码" />
                    )}
                  </FormItem>
                </div>
                : ''
            }
            <FormItem {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" >
                更新
                    </Button>
              {/* <Button htmlType="reset" >
                重设
                    </Button> */}
            </FormItem>
          </Form>
        </TabPane>
      </Tabs>
    )
  }
}
const WrappedNormalChpwd = Form.create()(Chpwd);

export default (WrappedNormalChpwd);