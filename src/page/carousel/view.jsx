import React, { Component } from "react";
import {
  Form,
  Input,
  Table,
  Select,
  Tabs,
  Button,
  Pagination,
  Tag,
  Modal,
  Row,
  Col,
  message,
  Cascader
} from "antd";
import { hot } from "react-hot-loader";
import options from "./cascader-address-options";
import { UPLOAD_IMG, API_ROOT_FILE } from "../../axios/config";

import {
  categorycreate,
  carouselsearch,
  categoryupdate,
  carouseldelete
} from "../../api";

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;

class Carousel extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      operating: "search",
      imgeurl: [],
      carousel: [],
      shopimg: [],
      blicense: [],
      width: 520,
      record: {},
      clientPage: 1
    };
    this.clientPage = 1;
    this.columnsForCategory = [
      {
        title: "id",
        dataIndex: "id",
        key: "id",
        className: "hidden"
      },
      {
        title: "图片",
        dataIndex: "img",
        key: "img",
        render: text => (
          <img style={{ width: "100px" }} src={API_ROOT_FILE + text} />
        )
      },
      {
        title: "城市",
        dataIndex: "city",
        key: "city"
      },
      {
        title: "跳转店铺名",
        dataIndex: "shop_name",
        key: "shop_name"
      },
      {
        title: "操作",
        dataIndex: "operation",
        key: "operation",
        render: (text, record, index) => {
          return (
            <div>
              <Button onClick={() => this.edit(record)}>编辑</Button>
              <Button onClick={() => this.delete(record.id)} type="danger">
                删除
              </Button>
            </div>
          );
        }
      }
    ];
  }

  componentDidMount() {
    try {
      const clientPage = this.props.location.query.clientPage;
      this.init({ clientPage });
    } catch (e) {
      this.init();
    }
  }
  init = (filter = {}) => {
    const req = {
      clientPage: 1,
      everyPage: 10,
      ...filter
    };
  
    carouselsearch(req).then(
      res => {
        const paginationForShop = {
          showQuickJumper: true,
          total: res.pager.sumpage,
          defaultPageSize: res.pager.everypage,
          onChange: page => {
            req.clientPage = page;
            carouselsearch(req).then(res => {
              this.setState({
                dataForShop: res.data,
                clientPage: page
              });
              this.clientPage = page;
            });
          }
        };

        this.setState({
          dataForShop: res.data,
          paginationForShop,
          clientPage: res.pager.clientpage
        });
        this.clientPage = res.pager.clientpage;
      },
      () => {
        this.setState({
          dataForShop: [],
          paginationForShop: {}
        });
      }
    );
  };

  /**
   * @description { 表单重置 }
   */
  handleReset = () => {
    this.props.form.resetFields();
  };
  /**
   * @description { 表单提交 }
   * @param e { 事件 }
   */
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.init({ city: values.city.join("/") });
      }
    });
  }

  /**
   * @description { 删除 }
   * @param key { id }
   */
  delete(id) {
    carouseldelete(id).then(() => {
      this.init({
        clientPage: this.state.clientPage
      });
    });
  }
  /**
   * @description { 编辑 }
   * @param key { id }
   */
  edit = record => {
    const { clientPage } = this.state;
    this.props.history.push({
      pathname: "/app/carousel/edit",
      query: { data: record, clientPage }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab="查看轮播图" key="1">
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <FormItem label="城市">
              {getFieldDecorator("city")(
                <Cascader
                  style={{ width: "100%" }}
                  placeholder="请选择！"
                  options={options}
                />
              )}
            </FormItem>
            <FormItem>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button onClick={this.handleReset}>重置</Button>
            </FormItem>
            <hr />
          </Form>
          <Table
            pagination={false}
            rowKey="id"
            dataSource={this.state.dataForShop}
            columns={this.columnsForCategory}
          />
          <hr />
          <Pagination
            current={+this.state.clientPage}
            style={{ textAlign: "right" }}
            {...this.state.paginationForShop}
          />
        </TabPane>
      </Tabs>
    );
  }
}
const WrappedNormalCategory = Form.create()(Carousel);

export default hot(module)(WrappedNormalCategory);
