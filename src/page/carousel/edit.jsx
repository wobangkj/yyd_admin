import React, { Component } from "react";

import {
  Tabs,
  Input,
  Button,
  Form,
  Icon,
  Upload,
  Modal,
  message,
  Cascader,
  Select
} from "antd";
import { hot } from "react-hot-loader";
import {
  setupupdate,
  shopsearch,
  carouselcreate,
  carouselupdate
} from "../../api";
import { UPLOAD_IMG, API_ROOT_FILE } from "../../axios/config";
import options from "./cascader-address-options";

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;
class ColumnAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      formItemLayout: {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 2 }
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 12 }
        }
      },
      tailFormItemLayout: {
        wrapperCol: {
          xs: {
            span: 24,
            offset: 0
          },
          sm: {
            span: 16,
            offset: 2
          }
        }
      },
      residencesCategory: [],
      previewVisible: false,
      previewImage: "",
      fileList: [],
      dataForShop: []
    };
  }

  init = (filter = {}) => {
    const req = {
      clientPage: 1,
      everyPage: 99999,
      ...filter
    };
    shopsearch(req).then(res => {
      this.setState({
        dataForShop: res.data
      });
    });
  };
  componentWillMount() {
    this.init();
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        try {
          const data = this.props.location.query.data;
          carouselupdate({
            ...values,
            img: this.state.fileList.map(item => item.response.filename)[0],
            city: values.city.join("/"),
            shop_id: values.uri,
            id: data.id
          });
        } catch (e) {}
      }
    });
  };
  handleReset = () => {
    this.props.form.resetFields();
    this.setState({
      fileList: []
    });
  };

  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = file => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true
    });
  };

  beforeUpload(file) {
    // const isJPG = file.type === 'image/jpeg';
    // if (!isJPG) {
    //   message.error('You can only upload JPG file!');
    // }
    const isLt2M = file.size / 1024 / 1024 < 1;
    if (!isLt2M) {
      message.error("图片最大不能超过1MB!");
    }
    // return isJPG && isLt2M;
    return isLt2M;
  }
  handleChange = ({ fileList }) => this.setState({ fileList });

  goBack = () => {
    try {
      const clientPage = this.props.location.query.clientPage;
      this.props.history.push({
        pathname: "/app/carousel/view",
        query: { clientPage }
      });
    } catch (e) {}
  };
  componentDidMount() {
    let data, fileList;
    try {
      data = this.props.location.query.data;
      fileList = [
        {
          url: API_ROOT_FILE + data.img,
          response: {
            filename: data.img
          },
          uid: 0
        }
      ];
    } catch (e) {
      data = {};
      fileList = [];
    }
    this.setState({
      data,
      fileList
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      previewVisible,
      previewImage,
      fileList,
      dataForShop,
      data
    } = this.state;

    let fileLists = [];
    fileList &&
      fileList.filter(item => {
        if (item.status === "uploading" || item.response) {
          fileLists.push(item);
        }
      });

    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">上传</div>
      </div>
    );

    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab="添加轮播图" key="1">
          <Form onSubmit={this.handleSubmit}>
            <FormItem {...this.state.formItemLayout} label="省市">
              {getFieldDecorator("city", {
                rules: [{ required: true, message: "请选择省市" }],
                initialValue: data.city && data.city.split("/")
              })(
                <Cascader
                  style={{ width: "100%" }}
                  placeholder="请选择！"
                  options={options}
                />
              )}
            </FormItem>
            <FormItem {...this.state.formItemLayout} label="跳转商家">
              {getFieldDecorator("uri", {
                rules: [{ required: true, message: "请选择跳转商家" }],
                initialValue: data.shop_id
              })(
                <Select placeholder="请选择跳转商家">
                  {dataForShop &&
                    dataForShop.map(item => (
                      <Option value={item.id} key={item.id}>
                        {item.name}
                      </Option>
                    ))}
                </Select>
              )}
            </FormItem>
            <FormItem {...this.state.formItemLayout} label="轮播图">
              {getFieldDecorator("img", {})(
                <div className="clearfix">
                  <Upload
                    action={UPLOAD_IMG}
                    listType="picture-card"
                    fileList={fileLists}
                    accept="image/gif,image/jpeg,image/jpg,image/png,image/svg"
                    beforeUpload={this.beforeUpload}
                    onPreview={this.handlePreview}
                    onChange={this.handleChange}
                    data={{ width: 476 }}
                  >
                    {fileLists.length >= 1 ? null : uploadButton}
                  </Upload>
                  <Modal
                    visible={previewVisible}
                    footer={null}
                    onCancel={this.handleCancel}
                  >
                    <img
                      alt="预览图"
                      style={{ width: "100%" }}
                      src={previewImage}
                    />
                  </Modal>
                </div>
              )}
            </FormItem>
            <FormItem {...this.state.tailFormItemLayout}>
              <Button type="primary" htmlType="submit">
                提交
              </Button>
              {data.city ? (
                <Button onClick={this.goBack}>返回</Button>
              ) : (
                <Button htmlType="reset">重设</Button>
              )}
            </FormItem>
          </Form>
        </TabPane>
      </Tabs>
    );
  }
}
const WrappedNormalColumnAdd = Form.create()(ColumnAdd);

export default hot(module)(WrappedNormalColumnAdd);
