import React, { Component } from 'react';
import createBrowserHistory from 'history/createBrowserHistory'
import '../../style/css/404.css'
import { hot } from 'react-hot-loader'

const history = createBrowserHistory()
class NotFound extends Component {

    render(){
        return(
            <div className="not-found" onClick={history.goBack}>
                <div className="tips" >
                    <h1>404</h1>
                    <h5>你来到了没有知识的荒漠</h5>
                    <h5>点击任意位置返回上一页</h5>
                </div>
            </div>
        )
    }
}

export default hot(module)(NotFound);