import React, { Component } from "react";
import {
  Form,
  Input,
  Table,
  Tabs,
  Button,
  Select,
  Pagination,
  Tag,
  Modal,
  Row,
  Col,
  message,
  DatePicker
} from "antd";
import { hot } from "react-hot-loader";
import { ordertrans, ordersearch, shoprecreate, shopresearch } from "../../api";
import moment from "moment";
const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;

const dateFormat = "YYYY-MM-DD";
const formItemLayout = {
  labelCol: {
    xs: { span: 6 },
    sm: { span: 6 }
  },
  wrapperCol: {
    xs: { span: 16 },
    sm: { span: 16 }
  }
};

class App extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      operating: "search",
      width: 520,
      record: {},
      clientPage: 1,
      selectedRowKeys: []
    };
    // this.clientPage = 1;
    this.columnsForTransfer = [
      {
        title: "id",
        dataIndex: "id",
        key: "id",
        className: "hidden"
      },
      {
        title: "转账日期",
        dataIndex: "createtime",
        key: "createtime"
      },
      {
        title: "转账人",
        dataIndex: "name",
        key: "name"
      },

      {
        title: "转账金额",
        dataIndex: "money",
        key: "money"
      },
      {
        title: "备注",
        dataIndex: "remark",
        key: "remark"
      }
    ];

    this.columnsForShop = [
      {
        title: "id",
        dataIndex: "id",
        key: "id",
        className: "hidden"
      },
      {
        title: "下单时间",
        dataIndex: "orderdate",
        key: "orderdate"
      },
      {
        title: "状态",
        dataIndex: "status",
        key: "status",
        render: text => {
          return text === 1 ? (
            <Tag color="green">已付款</Tag>
          ) : text === 2 ? (
            <Tag color="blue">已验证</Tag>
          ) : (
            <Tag color="red">未验证</Tag>
          );
        }
      },
      {
        title: "昵称",
        dataIndex: "client_name",
        key: "client_name"
      },
      {
        title: "用户名",
        dataIndex: "user_name",
        key: "user_name"
      },

      {
        title: "店铺",
        dataIndex: "shop_name",
        key: "shop_name"
      },
      {
        title: "优惠",
        dataIndex: "coupon_decprice",
        key: "coupon_decprice"
      },
      {
        title: "付款",
        dataIndex: "now_price",
        key: "now_price"
      },
      {
        title: "抽成比例",
        dataIndex: "platpro",
        key: "platpro",
        render: text => text + "%"
      },
      {
        title: "抽成金额",
        dataIndex: "draw_price",
        key: "draw_price"
      },
      {
        title: "应转账",
        dataIndex: "should_price",
        key: "should_price"
      },
      {
        title: "转账状态",
        dataIndex: "is_trans",
        key: "is_trans",
        render: text => {
          return text === 0 ? (
            <Tag color="blue">未转账</Tag>
          ) : (
            <Tag color="red">已转账</Tag>
          );
        }
      },
      {
        title: "操作",
        dataIndex: "operation",
        key: "operation",
        render: (text, record, index) => {
          return (
            <div>
              <Button onClick={() => this.view(record)}>查看</Button>
            </div>
          );
        }
      }
    ];
  }

  componentWillMount() {
    this.init();
  }

  init = (filter = {}) => {
    const req = {
      // clientPage: 1,
      // everyPage: 10,
      every: "all",
      status: "pay",
      ...filter
    };
    ordersearch(req).then(
      res => {
        const paginationForShop = {
          showQuickJumper: true,
          // total: res.pager.sumpage,
          defaultPageSize: 10
          // onChange: page => {
          //   req.clientPage = page;
          //   ordersearch(req).then(res => {
          //     this.setState({
          //       dataForShop: res.data,
          //       clientPage: page
          //     });
          //     // console.log(this.clientPage);
          //     // this.clientPage = page;
          //     // console.log(this.clientPage);
          //   });
          // }
        };

        this.setState({
          dataForShop: res.data,
          paginationForShop,
          clientPage: 10
        });
        // this.clientPage = res.pager.clientpage;
      },
      () => {
        this.setState({
          dataForShop: [],
          paginationForShop: {}
        });
      }
    );
  };

  /**
   * @description { 表单重置 }
   */
  handleReset = () => {
    this.props.form.resetFields();
  };
  /**
   * @description { 表单提交 }
   * @param e { 事件 }
   */
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (this.state.operating === "search") {
          let orderdate = "";
          let key = "";
          if (values.year) {
            orderdate = values.year;
          }
          if (values.month) {
            orderdate += "-" + values.month;
          }

          if (values.orderdata) {
            orderdate = moment(values.orderdata).format("YYYY-MM-DD");
          }
          if (values.s) {
            key = values.s + " " + orderdate;
          } else {
            key = orderdate;
          }
          this.init({
            key,
            status: values.status ? values.status : "pay"
            // orderdate
          });
        } else if (this.state.operating === "transfer") {
          ordertrans({
            ids: this.state.selectedRowKeys.join(",")
          }).then(() => {
            this.init({
              clientPage: this.state.clientPage
            });
          });
          shoprecreate({
            order_ids: this.state.selectedRowKeys.join(","),
            shop_id: this.state.record.shop_id,
            name: values.name,
            money: this.state.should_price,
            remark: values.remark
          }).then(() => {
            this.transferRecord(this.state.shop_id, this.state.record);
          });
        }
      }
    });
  }

  hideModal = () => {
    this.setState({
      visible: false,
      operating: "search",
      width: 520
    });
  };

  /**
   * @description { 查看 }
   * @param record {  }
   */
  view = record => {
    this.setState({
      visible: true,
      title: "查看",
      operating: "view",
      record
    });
  };

  /**
   * @description { 查看 }
   * @param record {  }
   */
  transfer = () => {
    const { allRecord } = this.state;
    if (!allRecord || allRecord.length === 0) {
      message.warn("请选择订单");
      return;
    }
    const shop_id = allRecord[0].shop_id;

    if (allRecord.some(item => item.shop_id !== shop_id)) {
      message.warn("批量转账只允许对同一个商家处理");
      return;
    }

    if (allRecord.some(item => item.is_trans !== 0)) {
      message.warn("存在已转账订单");
      return;
    }

    let should_price = 0;
    allRecord.map(item => {
      should_price += item.should_price;
    });
    console.log(allRecord);

    this.setState({
      visible: true,
      title: "转账",
      shop_id,
      operating: "transfer",
      should_price: should_price.toFixed(4),
      record: allRecord[0]
    });
  };
  transferRecord = (shop_id, record) => {
    shopresearch({
      clientPage: 1,
      everyPage: 99999,
      shop_id
    }).then(res => {
      this.setState({
        operating: "transferRecord",
        visible: true,
        name: record.name,
        shop_paycard: record.shop_paycard,
        shop_paykind: record.shop_paykind,
        shop_payname: record.shop_payname,
        width: 1000,
        title: "查看转账记录",
        dataForTransfer: res.data
      });
    });
  };
  /**
   * @description { 弹窗点击取消事件 }
   */
  handleCancel = () => {
    this.setState({
      previewVisible: false
    });
  };
  onSelectChange = (selectedRowKeys, allRecord) => {
    console.log(selectedRowKeys, allRecord);
    this.setState({ selectedRowKeys, allRecord });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    };
    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab="订单查看" key="1">
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <FormItem>
              {getFieldDecorator("s", {})(
                <Input addonBefore="店铺名" placeholder="请输入店铺名" />
              )}
            </FormItem>
            <FormItem label="年份">
              {getFieldDecorator("year", {
                rules: [{ pattern: /^\d{4}$/, message: "年份格式非法" }]
              })(<Input placeholder="请输入年份" />)}
            </FormItem>
            <FormItem label="月份">
              {getFieldDecorator("month", {})(
                <Select placeholder="请选择月份" style={{ width: "250px" }}>
                  <Option key={0} value="01">
                    一月
                  </Option>
                  <Option key={1} value="02">
                    二月
                  </Option>
                  <Option key={2} value="03">
                    三月
                  </Option>
                  <Option key={3} value="04">
                    四月
                  </Option>
                  <Option key={4} value="05">
                    五月
                  </Option>
                  <Option key={5} value="06">
                    六月
                  </Option>
                  <Option key={6} value="07">
                    七月
                  </Option>
                  <Option key={7} value="08">
                    八月
                  </Option>
                  <Option key={8} value="09">
                    九月
                  </Option>
                  <Option key={9} value="10">
                    十月
                  </Option>
                  <Option key={10} value="11">
                    十一月
                  </Option>
                  <Option key={11} value="12">
                    十二月
                  </Option>
                </Select>
              )}
            </FormItem>
            <FormItem label="状态">
              {getFieldDecorator("status", {})(
                <Select placeholder="请选择状态" style={{ width: "250px" }}>
                  <Option key={1} value={1}>
                    已付款
                  </Option>
                  <Option key={2} value={2}>
                    已验证
                  </Option>
                  <Option key={8} value={8}>
                    未验证
                  </Option>
                </Select>
              )}
            </FormItem>
            <FormItem label="年月日查询">
              {getFieldDecorator("orderdata", {})(
                <DatePicker format={dateFormat} />
              )}
            </FormItem>

            <FormItem>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
              <Button onClick={this.handleReset}>重置</Button>
              <Button onClick={this.transfer}>转账</Button>
            </FormItem>
            <hr />
          </Form>
          <Table
            // pagination={false}
            rowSelection={rowSelection}
            rowKey="id"
            scroll={{ x: 1500 }}
            dataSource={this.state.dataForShop}
            columns={this.columnsForShop}
          />
          <hr />
          {/* <Pagination
            current={this.state.clientPage}
            style={{ textAlign: "right" }}
            {...this.state.paginationForShop}
          /> */}

          <Modal
            title={this.state.title}
            footer={null}
            width={this.state.width}
            visible={this.state.visible}
            onCancel={this.hideModal}
          >
            {this.state.operating === "view" && (
              <Row>
                <Col span={12} style={{ paddingBottom: 10 }}>
                  客户姓名：{this.state.record.client_name}
                </Col>
                <Col span={12} style={{ paddingBottom: 10 }}>
                  客户手机号：{this.state.record.client_phone}
                </Col>
                <Col span={12} style={{ paddingBottom: 10 }}>
                  特约技师：{this.state.record.staff_name}
                </Col>
                <Col span={12} style={{ paddingBottom: 10 }}>
                  技师编号：{this.state.record.staff_number}
                </Col>
                <Col span={24} style={{ paddingBottom: 10 }}>
                  预约项目：{this.state.record.service_name}
                </Col>
                <Col span={24} style={{ paddingBottom: 10 }}>
                  预约时间：
                  {this.state.record.orderdate +
                    " " +
                    this.state.record.starttime}
                </Col>
                <Col span={24} style={{ paddingBottom: 10 }}>
                  订单编号：{this.state.record.order_num}
                </Col>
                <Col span={24} style={{ paddingBottom: 10 }}>
                  价格：{this.state.record.origin_price}
                </Col>
                <Col span={24} style={{ paddingBottom: 10 }}>
                  服务收费：{this.state.record.service_price}
                </Col>
                <Col span={24} style={{ paddingBottom: 10 }}>
                  优惠：{this.state.record.coupon_decprice}
                </Col>
                <Col
                  span={24}
                  style={{ paddingBottom: 10, textAlign: "right" }}
                >
                  共计：{this.state.record.origin_price}
                </Col>
              </Row>
            )}

            {this.state.operating === "transfer" && (
              <Form onSubmit={this.handleSubmit}>
                <Row>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    服务商名称：{this.state.record.shop_name}
                  </Col>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    服务商账户名：{this.state.record.shop_payname}
                  </Col>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    开户行：{this.state.record.shop_paykind}
                  </Col>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    账号：{this.state.record.shop_paycard}
                  </Col>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    应转账：{this.state.should_price}
                  </Col>
                </Row>
                <hr />
                <FormItem label="经办人" {...formItemLayout}>
                  {getFieldDecorator("name", {
                    rules: [{ required: true, message: "请输入经办人" }]
                  })(
                    <Input placeholder="请输入经办人" style={{ width: 350 }} />
                  )}
                </FormItem>
                <FormItem label="备注" {...formItemLayout}>
                  {getFieldDecorator("remark", {
                    // rules: [{ required: true, message: '请输入备注' }],
                  })(<Input placeholder="请输入备注" style={{ width: 350 }} />)}
                </FormItem>
                <Row>
                  <Col style={{ textAlign: "center", paddingTop: 50 }}>
                    <Button type="primary" htmlType="submit">
                      提交
                    </Button>
                    <Button onClick={this.handleReset}>重置</Button>
                  </Col>
                </Row>
              </Form>
            )}

            {this.state.operating === "transferRecord" && (
              <div>
                <Row style={{ paddingBottom: 10 }}>
                  <Col span={12}>服务商名称：{this.state.name}</Col>
                  <Col span={12}>服务商账户名：{this.state.shop_payname}</Col>
                </Row>
                <Row style={{ paddingBottom: 10 }}>
                  <Col span={12}>开户行：{this.state.shop_paykind}</Col>
                  <Col span={12}>账号：{this.state.shop_paycard}</Col>
                </Row>
                <hr />
                <Table
                  rowKey="id"
                  dataSource={this.state.dataForTransfer}
                  columns={this.columnsForTransfer}
                />
              </div>
            )}
          </Modal>
        </TabPane>
      </Tabs>
    );
  }
}

export default hot(module)(Form.create()(App));
