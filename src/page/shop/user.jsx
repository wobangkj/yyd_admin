import React, { Component } from "react";
import {
  Form,
  Input,
  Table,
  Select,
  Tabs,
  Button,
  Pagination,
  Tag,
  Modal,
  Row,
  Col,
  Cascader,
  message
} from "antd";
import { hot } from "react-hot-loader";
import { admincreate, adminsearch, adminupdate, admindelete, getCode, setPhone,getPhone } from "../../api";
import options from "./cascader-address-options";

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;

class Category extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      operating: "search",
      imgeurl: [],
      carousel: [],
      shopimg: [],
      blicense: [],
      width: 520,
      record: {},
      clientPage: 1,
      rowId: '',
      codeType:'',//判断短信模板
      checked: false,//判断验证码按钮是否点击
      showPhone: false,//判断添加和编辑是否显示手机号
      yzPhone: false,//判断手机号验证弹框的显示
      time: 60,//验证码倒计时初始时间
      loginPhone:'',//登录的手机号
    };
    // this.clientPage = 1
    this.columnsForCategory = [
      {
        title: "id",
        dataIndex: "id",
        key: "id",
        className: "hidden"
      },
      {
        title: "账号",
        dataIndex: "account",
        key: "account"
      },

      {
        title: "代理区域",
        dataIndex: "area",
        key: "area",
        render: text => {
          return text;
        }
      },
      {
        title: "分类等级",
        dataIndex: "role",
        key: "role",
        render: text => {
          return text === 0 ? (
            <Tag color="blue">代理</Tag>
          ) : (
              <Tag color="red">管理员</Tag>
            );
        }
      },
      {
        title: "操作",
        dataIndex: "operation",
        key: "operation",
        render: (text, record, index) => {
          return (
            <div>
              <Button onClick={() => this.edit(record.id, record)}>编辑</Button>
              <Button onClick={() => this.delete(record.id, record)} type="danger">
                删除
              </Button>
            </div>
          );
        }
      }
    ];
  }

  componentWillMount() {
    this.init();
  }

  showModal = () => {
    this.handleReset();

    this.setState({
      visible: true,
      record: {},
      id: "",
      checked: false,
      showPhone: true,
    });
  };

  init = (filter = {}) => {
    const req = {
      clientPage: 1,
      everyPage: 10,
      ...filter
    };
    adminsearch(req).then(
      res => {
        const paginationForShop = {
          showQuickJumper: true,
          total: res.pager.sumpage,
          defaultPageSize: res.pager.everypage,
          onChange: page => {
            req.clientPage = page;
            adminsearch(req).then(res => {
              this.setState({
                dataForShop: res.data,
                clientPage: page
              });
              // this.clientPage = page
            });
          }
        };

        this.setState({
          dataForShop: res.data,
          paginationForShop,
          clientPage: res.pager.clientpage
        });
        // this.clientPage = res.pager.clientpage
      },
      () => {
        this.setState({
          dataForShop: [],
          paginationForShop: {}
        });
      }
    );
  };

  /**
   * @description { 表单重置 }
   */
  handleReset = () => {
    this.props.form.resetFields();
  };
  /**
   * @description { 表单提交 }
   * @param e { 事件 }
   */
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.area = values.area.join("/");
        if (this.state.id) {
          adminupdate({
            ...values,
            id: this.state.id
          }).then(() => {
            this.init({
              clientPage: this.state.clientPage
            });
            this.hideModal();
          });
        } else {
          admincreate(values).then((res) => {
            if (res.status === 201) {
              this.init({
                clientPage: this.state.clientPage
              });
              this.hideModal();
            }else{
              message.error(res.msg)
            }
          });
        }
      }
    });
  }

  hideModal = () => {
    this.setState({
      visible: false,
      width: 520
    });
  };
  /**
   * @description { 删除 }
   * @param key { id }
   */
  delete(id, record) {
    getPhone({
      account: sessionStorage.getItem('username')
    }).then(res => {
      this.setState({
        loginPhone: res.data.phone
      })
    })
    this.setState({
      yzPhone: true,
      rowId: id,
      codeType:'delete_proxy',
      record
    })

  }
  /**
   * @description { 编辑 }
   * @param key { id }
   */
  edit = (id, record) => {
    console.log(record)
    this.setState({
      id,
      visible: true,
      title: "编辑",
      record,
      checked: false,
      showPhone: false
    });
  };

  /**手机验证点击确定 */
  handleOk() {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const req = {
          account: values.account,
          phone: values.phone,
          code: values.code
        }
        setPhone(req).then(res => {
          if (res.status === 212) {
            admindelete(this.state.rowId).then(() => {
              this.init({
                clientPage: this.state.clientPage
              });
            });
            this.props.form.resetFields('code')
            this.setState({
              yzPhone: false,
              visible: false
            })
          } else {
            message.error(res.msg, 0.5);
          }
        })
      }
    })
  }
  /**获取验证码 */
  getCode() {
    const phone = this.props.form.getFieldValue('phone')
    this.countdown()
    this.setState({
      checked: true
    })
    getCode({
      phone: phone,
      type: this.state.codeType
    }).then(res => {
      message.success('验证码发送成功', 0.5)
    })
  }
  //验证码倒计时
  countdown() {
    let that = this
    let time = this.state.time
    if (time != 0) {
      this.setState({
        time: time - 1
      })
      setTimeout(() => {
        that.countdown()
      }, 1000);
    }
    if (time == 0) {
      this.setState({
        checked: false,
        time: 60
      })
    }
  }
  timeTip() {
    message.error(`请${this.state.time}后再次尝试`);
  }

  /**
   * @description { 弹窗点击取消事件 }
   */
  handleCancel = () => {
    this.setState({
      previewVisible: false,
      yzPhone: false
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab="查看服务商" key="1">
          <Button type="primary" onClick={this.showModal}>
            添加
          </Button>
          <Table
            pagination={false}
            rowKey="id"
            dataSource={this.state.dataForShop}
            columns={this.columnsForCategory}
          />
          <hr />
          <Pagination
            current={this.state.clientPage}
            style={{ textAlign: "right" }}
            {...this.state.paginationForShop}
          />

          <Modal
            title={this.state.title}
            footer={null}
            width={this.state.width}
            visible={this.state.visible}
            onCancel={this.hideModal}
          >
            {this.state.visible && (
              <Form onSubmit={this.handleSubmit}>
                <FormItem label="账号">
                  {getFieldDecorator("account", {
                    rules: [{ required: true, message: "请输入" }],
                    initialValue: this.state.record.account
                  })(<Input placeholder="请输入账号" />)}
                </FormItem>
                <FormItem label="密码">
                  {getFieldDecorator("password", {
                    // rules: [{ required: true, message: "请输入" }]
                  })(<Input placeholder="请输入密码" />)}
                </FormItem>
                <FormItem label="用户角色">
                  {getFieldDecorator("role", {
                    rules: [{ required: true, message: "请输入" }],
                    initialValue: this.state.record.role
                  })(
                    <Select>
                      <Option key={0} value={0}>
                        代理
                      </Option>
                      <Option key={1} value={1}>
                        管理员
                      </Option>
                    </Select>
                  )}
                </FormItem>
                <FormItem label="区域">
                  {getFieldDecorator("area", {
                    rules: [{ required: true, message: "请输入" }],
                    initialValue:
                      this.state.record.area &&
                      this.state.record.area.split("/")
                  })(
                    <Cascader
                      style={{ width: "100%" }}
                      placeholder="请选择！"
                      options={options}
                    />
                  )}
                </FormItem>
                {
                  this.state.showPhone === true ?
                    <FormItem label="手机号">
                      <Row gutter={8}>
                        <Col span={12}>
                          {getFieldDecorator('phone', {
                            rules: [{ required: true, message: "请输入" }],
                          })(<Input />)}
                        </Col>
                        <Col span={6}>
                          {
                            this.state.checked == true ?
                              <Button onClick={this.timeTip.bind(this)}>{this.state.time}后再次获取验证码</Button> :
                              <Button onClick={this.getCode.bind(this)}>获取验证码</Button>
                          }
                        </Col>
                      </Row>
                    </FormItem> : ''
                }
                {
                  this.state.showPhone === true ?
                    <FormItem label="验证码">
                      {getFieldDecorator("code", {
                        initialValue: this.state.record.phone,
                        rules: [{ required: true, message: "请输入" }],
                      })(<Input placeholder="请输入验证码" />)}
                    </FormItem> : ''
                }

                <hr />

                <Row>
                  <Col style={{ textAlign: "center", paddingTop: 50 }}>
                    <Button type="primary" htmlType="submit">
                      提交
                    </Button>
                    <Button onClick={this.handleReset}>重置</Button>
                  </Col>
                </Row>
              </Form>
            )}
          </Modal>

          <Modal
            title="手机号验证"
            visible={this.state.yzPhone}
            onOk={this.handleOk.bind(this)}
            onCancel={this.handleCancel.bind(this)}
          >
            <Form>
              <FormItem label="手机号">
                <Row gutter={8}>
                  <Col span={12}>
                    {getFieldDecorator('phone', {
                      initialValue: this.state.loginPhone,
                      rules: [{ required: true, message: '请输入手机号！' }],
                    })(<Input disabled />)}
                  </Col>
                  <Col span={6}>
                    {
                      this.state.checked == true ?
                        <Button onClick={this.timeTip.bind(this)}>{this.state.time}后再次获取验证码</Button> :
                        <Button onClick={this.getCode.bind(this)}>获取验证码</Button>
                    }
                  </Col>
                </Row>
              </FormItem>
              <FormItem label="验证码">
                {getFieldDecorator('code', {
                  rules: [
                    { required: true, message: '验证码不能为空！' }
                  ],
                })(
                  <Input type="text" placeholder="请输入验证码" />
                )}
              </FormItem>
            </Form>
          </Modal>
        </TabPane>
      </Tabs>
    );
  }
}
const WrappedNormalCategory = Form.create()(Category);

export default hot(module)(WrappedNormalCategory);
