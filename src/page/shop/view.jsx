import React, { Component } from "react";
import {
  Form,
  Input,
  Table,
  Select,
  Tabs,
  Button,
  Pagination,
  Tag,
  Modal,
  Row,
  Col,
  Upload,
  Icon,
  message,
  InputNumber
} from "antd";
import { hot } from "react-hot-loader";
import {
  shopsearch,
  categorysearchx,
  shopupdate,
  shopresearch,
  shopdelete,
  getCode,
  setPhone,
  getPhone
} from "../../api";
import { UPLOAD_IMG, API_ROOT_FILE } from "../../axios/config";

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 6 },
    sm: { span: 6 }
  },
  wrapperCol: {
    xs: { span: 16 },
    sm: { span: 16 }
  }
};

class Category extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      operating: "search",
      imgeurl: [],
      carousel: [],
      shopimg: [],
      blicense: [],
      width: 520,
      clientPage: 1,
      record: {},
      rowId:'',//点击行的id
      yzPhone:false,//控制验证手机号弹框的显示
      loginPhone:''
    };
    // this.clientPage = 1;

    this.columnsForTransfer = [
      {
        title: "id",
        dataIndex: "id",
        key: "id",
        className: "hidden"
      },
      {
        title: "转账日期",
        dataIndex: "createtime",
        key: "createtime"
      },
      {
        title: "转账人",
        dataIndex: "name",
        key: "name"
      },
      {
        title: "转账金额",
        dataIndex: "money",
        key: "money"
      },
      {
        title: "备注",
        dataIndex: "remark",
        key: "remark"
      }
    ];

    this.columnsForShop = [
      {
        title: "id",
        dataIndex: "id",
        key: "id",
        className: "hidden"
      },
      {
        title: "服务商名称",
        dataIndex: "name",
        key: "name"
      },
      {
        title: "服务商分类",
        dataIndex: "category",
        key: "category"
      },
      {
        title: "服务商地址",
        dataIndex: "address",
        key: "address"
      },
      {
        title: "服务商状态",
        dataIndex: "status",
        key: "status",
        render: text => {
          return text === 0 ? (
            <Tag color="blue">待审核</Tag>
          ) : text === 1 ? (
            <Tag color="green">营业中</Tag>
          ) : text === 6 ? (
            <Tag color="orange">审核通过</Tag>
          ) : (
            <Tag color="red">审核未通过</Tag>
          );
        }
      },
      {
        title: "成交单数",
        dataIndex: "order_num",
        key: "order_num"
      },
      {
        title: "付款总金额(元)",
        dataIndex: "allmoney",
        key: "allmoney"
      },
      {
        title: "优惠",
        dataIndex: "discount",
        key: "discount"
      },
      {
        title: "平台抽成比例",
        dataIndex: "platpro",
        key: "platpro",
        render: text => text + "%"
      },
      {
        title: "账号",
        dataIndex: "paycard",
        key: "paycard"
      },
      {
        title: "操作",
        dataIndex: "operation",
        key: "operation",
        render: (text, record, index) => {
          return (
            <div>
              <Button onClick={() => this.transfer(record.id, record)}>
                查看转账记录
              </Button>

              {sessionStorage.getItem("role") == 1 && (
                <Button onClick={() => this.review(record.id, record)}>
                  审核
                </Button>
              )}
              <Button onClick={() => this.edit(record.id, record)}>编辑</Button>
              {/* <Button onClick={() => this.editPwd(record.id, record.password)}>
                修改密码
              </Button> */}
              <Button onClick={() => this.delete(record.id,record)} type="danger">
                删除
              </Button>
            </div>
          );
        }
      }
    ];
  }

  componentWillMount() {
    categorysearchx({
      every: "all",
      parent_id: 0
    }).then(res => {
      this.setState({
        categoryp: res.data
      });
    });

    categorysearchx({
      every: "all",
      flag: 1
    }).then(res => {
      this.setState({
        categoryc: res.data
      });
    });
    this.init();
  }

  changeCategory = parent_id => {
    categorysearchx({
      every: "all",
      parent_id
    }).then(res => {
      this.setState({
        category: res.data
      });
    });
  };
  init = (filter = {}) => {
    const req = {
      clientPage: 1,
      everyPage: 10,
      ...filter
    };
    shopsearch(req).then(
      res => {
        const paginationForShop = {
          showQuickJumper: true,
          total: res.pager.sumpage,
          defaultPageSize: res.pager.everypage,
          onChange: page => {
            req.clientPage = page;
            shopsearch(req).then(res => {
              this.setState({
                dataForShop: res.data,
                clientPage: page
              });
              // this.clientPage = page;
            });
          }
        };

        this.setState({
          dataForShop: res.data,
          paginationForShop,
          clientPage: res.pager.clientpage
        });
        // this.clientPage = res.pager.clientpage;
      },
      () => {
        this.setState({
          dataForShop: [],
          paginationForShop: {}
        });
      }
    );
  };

  review = (id, record) => {
    const blicense = record.blicense.split(",").map(item => ({
      url: API_ROOT_FILE + item,
      uid: item,
      status: "done",
      response: {
        filename: item
      }
    }));
    const shopimg = record.shopimg.split(",").map(item => ({
      url: API_ROOT_FILE + item,
      uid: item,
      status: "done",
      response: {
        filename: item
      }
    }));
    this.setState({
      id,
      record: {
        status: record.status
      },
      name: record.name,
      categoryShop: record.categoryp + " " + record.category,
      address: record.address,
      payname: record.payname,
      contact_name: record.contact_name,
      contact_phone: record.contact_phone,
      blicense,
      shopimg,
      operating: "review",
      visible: true,
      title: "商家审核管理"
    });
  };

  editPwd = (id, password) => {
    this.setState({
      id,
      record: {
        password
      },
      operating: "editPwd",
      visible: true,
      title: "修改密码"
    });
  };
  transfer = (shop_id, record) => {
    shopresearch({
      clientPage: 1,
      everyPage: 99999,
      shop_id
    }).then(res => {
      this.setState({
        operating: "transfer",
        visible: true,
        name: record.name,
        paycard: record.paycard,
        paykind: record.paykind,
        payname: record.payname,
        width: 1000,
        title: "查看转账记录",
        dataForTransfer: res.data
      });
    });
  };

  editShop = (filter = {}) => {
    const { record } = this.state;
    for (var key in filter) {
      if (record[key] === filter[key]) {
        delete filter[key];
      }
    }
    if (Object.keys(filter).length === 0) {
      message.warn("暂无修改数据!");
      return;
    }
    shopupdate({
      id: this.state.id,
      ...filter
    }).then(res => {
      if (res.status === 206) {
        this.init({
          clientPage: this.state.clientPage
        });
        this.hideModal();
      }
    });
  };
  /**
   * @description { 表单重置 }
   */
  handleReset = () => {
    this.props.form.resetFields();
  };
  /**
   * @description { 表单提交 }
   * @param e { 事件 }
   */
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (this.state.operating === "search") {
          let key = "";
          if (values.s_city && values.s_key) {
            key = values.s_city + " " + values.s_key;
          } else if (values.s_city) {
            key = values.s_city;
          } else if (values.s_key) {
            key = values.s_key;
          }
          this.init({
            key,
            status: values.s_status,
            category_id: values.s_category
          });
        } else if (this.state.operating === "review") {
          this.editShop({
            status: values.status
          });
        } else if (this.state.operating === "editPwd") {
          this.editShop({
            password: values.password
          });
        } else if (this.state.operating === "edit") {
          console.log(values);
          values.label = values._label;

          delete values._label;
          delete values.s_city;
          delete values.s_name;
          delete values.s_key;
          delete values.s_status;
          delete values.s_category;
          delete values.categoryp;

          try {
            values.imgeurl = values.imgeurl[0].response.filename;
          } catch (e) {
            delete values.imgeurl;
          }

          try {
            values.carousel = values.carousel.map(
              item => item.response.filename
            );
            values.carousel = values.carousel.join(",");
          } catch (e) {
            delete values.carousel;
          }

          try {
            values.blicense = values.blicense.map(
              item => item.response.filename
            );
            values.blicense = values.blicense.join(",");
          } catch (e) {
            delete values.blicense;
          }

          try {
            values.shopimg = values.shopimg.map(item => item.response.filename);
            values.shopimg = values.shopimg.join(",");
          } catch (e) {
            delete values.shopimg;
          }

          this.editShop(values);
        }
      }
    });
  }

  hideModal = () => {
    this.setState({
      visible: false,
      operating: "search",
      width: 520
    });
  };
  /**
   * @description { 删除 }
   * @param key { id }
   */
  delete(id,record) {
    getPhone({
      account: sessionStorage.getItem('username')
    }).then(res => {
      this.setState({
        loginPhone: res.data.phone
      })
    })
    this.setState({
      rowId:id,
      yzPhone:true,
      record
    })
  }

  /***
   * @description { 手机号验证 }
   */
  /**手机验证点击确定 */
  handleOk() {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const req = {
          account: values.account,
          phone: values.contact_phone,
          code: values.code
        }
        setPhone(req).then(res => {
          if (res.status === 212) {
            shopdelete(this.state.rowId).then(() => {
              this.setState({
                yzPhone:false
              })
              this.init({
                clientPage: this.state.clientPage
              });
            });
          } else {
            message.error(res.msg, 0.5);
          }
        })
      }
    })
  }
  /**获取验证码 */
  getCode() {
    const phone = this.props.form.getFieldValue('contact_phone')
    this.countdown()
    this.setState({
      checked: true
    })
    getCode({
      phone: phone,
      type: this.state.codeType
    }).then(res => {
      message.success('验证码发送成功', 0.5)
    })
  }
  //验证码倒计时
  countdown() {
    let that = this
    let time = this.state.time
    if (time != 0) {
      this.setState({
        time: time - 1
      })
      setTimeout(() => {
        that.countdown()
      }, 1000);
    }
    if (time == 0) {
      this.setState({
        checked: false,
        time: 60
      })
    }
  }
  timeTip() {
    message.error(`请${this.state.time}后再次尝试`);
  }
  /**
   * @description { 文件预览 }
   * @param file { 文件 }
   */
  handlePreview = file => {
    this.setState(
      {
        previewImage: file.url || file.thumbUrl,
        previewVisible: true
      },
      () => {
        console.log(this.state.previewImage);
      }
    );
  };
  /**
   * @description { 编辑 }
   * @param key { id }
   */
  edit = (id, record) => {
    record._label = record.label;
    const imgeurl =
      record.imgeurl &&
      record.imgeurl.split(",").map(item => ({
        url: API_ROOT_FILE + item,
        uid: item,
        status: "done",
        response: {
          filename: item
        }
      }));
    const carousel =
      record.carousel &&
      record.carousel.split(",").map(item => ({
        url: API_ROOT_FILE + item,
        uid: item,
        status: "done",
        response: {
          filename: item
        }
      }));
    const blicense =
      record.blicense &&
      record.blicense.split(",").map(item => ({
        url: API_ROOT_FILE + item,
        uid: item,
        status: "done",
        response: {
          filename: item
        }
      }));
    const shopimg = record.shopimg.split(",").map(item => ({
      url: API_ROOT_FILE + item,
      uid: item,
      status: "done",
      response: {
        filename: item
      }
    }));

    this.setState({
      id,
      shopimg,
      blicense,
      imgeurl,
      carousel,
      operating: "edit",
      visible: true,
      title: "编辑",
      record
    });
  };

  /**
   * @description { 图片处理 }
   * @param fileList { 文件列表 }
   */
  handleChangeImg({ fileList }, kind) {
    // eslint-disable-next-line
    switch (kind) {
      case "imgeurl":
        this.props.form.setFieldsValue({
          imgeurl: fileList
        });
        this.setState({
          imgeurl: fileList
        });
        break;
      case "carousel":
        this.props.form.setFieldsValue({
          carousel: fileList
        });
        this.setState({
          carousel: fileList
        });
        break;
      case "blicense":
        this.props.form.setFieldsValue({
          blicense: fileList
        });
        this.setState({
          blicense: fileList
        });
        break;
      case "shopimg":
        this.props.form.setFieldsValue({
          shopimg: fileList
        });
        this.setState({
          shopimg: fileList
        });
        break;
    }
  }
  /**
   * @description { 弹窗点击取消事件 }
   */
  handleCancel = () => {
    this.setState({
      previewVisible: false,
      yzPhone:false
    });
  };
  beforeUpload(file) {
    // const isJPG = file.type === 'image/jpeg';
    // if (!isJPG) {
    //   message.error('You can only upload JPG file!');
    // }
    const isLt2M = file.size / 1024 / 1024 < 1;
    if (!isLt2M) {
      message.error("图片最大不能超过1MB!");
    }
    // return isJPG && isLt2M;
    return isLt2M;
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { previewVisible, previewImage } = this.state;

    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">上传</div>
      </div>
    );

    const UploadFile = (fileList, kind, num) => {
      const fileLists = [];
      fileList &&
        fileList.filter(item => {
          if (item.status === "uploading" || item.response) {
            fileLists.push(item);
          }
        });
      return (
        <div>
          <Upload
            action={UPLOAD_IMG}
            listType="picture-card"
            fileList={fileLists}
            accept="image/gif,image/jpeg,image/jpg,image/png,image/svg"
            beforeUpload={this.beforeUpload}
            onPreview={data => this.handlePreview(data)}
            onChange={data => this.handleChangeImg(data, kind)}
          >
            {fileLists.length >= num ? null : uploadButton}
          </Upload>
          <Modal
            visible={previewVisible}
            footer={null}
            onCancel={this.handleCancel}
          >
            <img alt="预览图" style={{ width: 450 }} src={previewImage} />
          </Modal>
        </div>
      );
    };

    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab="查看服务商" key="1">
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <FormItem>
              {getFieldDecorator("s_key", {})(
                <Input addonBefore="关键字" placeholder="请输入关键字" />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("s_city", {})(
                <Input addonBefore="城市" placeholder="请输入城市名" />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("s_status", {})(
                <Select
                  placeholder="请选择服务商状态"
                  style={{ width: "250px" }}
                >
                  <Option key={0} value={0}>
                    待审核
                  </Option>
                  <Option key={1} value={1}>
                    营业中
                  </Option>
                  <Option key={6} value={6}>
                    审核通过
                  </Option>
                  <Option key={2} value={2}>
                    审核未通过
                  </Option>
                </Select>
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("s_category", {})(
                <Select
                  placeholder="请选择服务商分类"
                  style={{ width: "250px" }}
                >
                  {this.state.categoryc &&
                    this.state.categoryc.map(item => (
                      <Option key={item.id} value={item.id}>
                        {item.category}
                      </Option>
                    ))}
                </Select>
              )}
            </FormItem>
            <FormItem>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
              <Button onClick={this.handleReset}>重置</Button>
            </FormItem>
            <hr />
          </Form>
          <Table
            pagination={false}
            rowKey="id"
            scroll={{ x: 1500 }}
            dataSource={this.state.dataForShop}
            columns={this.columnsForShop}
          />
          <hr />
          <Pagination
            current={this.state.clientPage}
            style={{ textAlign: "right" }}
            {...this.state.paginationForShop}
          />

          <Modal
            title={this.state.title}
            footer={null}
            width={this.state.width}
            visible={this.state.visible}
            onCancel={this.hideModal}
          >
            {this.state.operating === "review" && (
              <Form layout="inline" onSubmit={this.handleSubmit}>
                <Row>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    店铺名称：{this.state.name}
                  </Col>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    店铺分类：{this.state.categoryShop}
                  </Col>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    店铺地址：{this.state.address}
                  </Col>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    营业执照：
                    {UploadFile(
                      this.state.blicense,
                      "",
                      this.state.blicense.length
                    )}
                  </Col>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    执业资质证书：
                    {UploadFile(
                      this.state.shopimg,
                      "",
                      this.state.shopimg.length
                    )}
                  </Col>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    联系人姓名：{this.state.contact_name}
                  </Col>
                  <Col span={24} style={{ paddingBottom: 10 }}>
                    联系人手机：{this.state.contact_phone}
                  </Col>
                </Row>
                <hr />
                <FormItem label="服务商状态">
                  {getFieldDecorator("status", {
                    rules: [{ required: true, message: "请选择服务商状态" }],
                    initialValue: this.state.record.status
                  })(
                    <Select
                      placeholder="请选择服务商状态"
                      style={{ width: 350 }}
                    >
                      <Option key={0} value={0}>
                        待审核
                      </Option>
                      <Option key={1} value={1}>
                        营业中
                      </Option>
                      <Option key={6} value={6}>
                        审核通过
                      </Option>
                      <Option key={2} value={2}>
                        审核未通过
                      </Option>
                    </Select>
                  )}
                </FormItem>
                <Row>
                  <Col style={{ textAlign: "center", paddingTop: 50 }}>
                    <Button type="primary" htmlType="submit">
                      提交
                    </Button>
                    <Button onClick={this.handleReset}>重置</Button>
                  </Col>
                </Row>
              </Form>
            )}
            {this.state.operating === "edit" && (
              <Form onSubmit={this.handleSubmit}>
                <FormItem label="服务商名" {...formItemLayout}>
                  {getFieldDecorator("name", {
                    rules: [{ required: true, message: "请输入服务商名" }],
                    initialValue: this.state.record.name
                  })(
                    <Input
                      placeholder="请输入服务商名"
                      style={{ width: 350 }}
                    />
                  )}
                </FormItem>
                <FormItem label="平台抽成比例" {...formItemLayout}>
                  {getFieldDecorator("platpro", {
                    rules: [{ required: true, message: "请输入平台抽成比例" }],
                    initialValue: this.state.record.platpro
                  })(
                    <InputNumber
                      defaultValue={100}
                      min={0}
                      max={100}
                      formatter={value => `${value}%`}
                      parser={value => value.replace("%", "")}
                      placeholder="请输入平台抽成比例"
                      style={{ width: 350 }}
                    />
                  )}
                </FormItem>
                {/* <FormItem label="店铺主分类" {...formItemLayout}>
                  {getFieldDecorator("categoryp", {
                    rules: [{ required: true, message: "请选择服务商主分类" }],
                    initialValue: this.state.record.categoryp
                  })(
                    <Select
                      onSelect={this.changeCategory}
                      placeholder="请选择服务商主分类"
                      style={{ width: 350 }}
                    >
                      {this.state.categoryp &&
                        this.state.categoryp.map(item => (
                          <Option key={item.id} value={item.id}>
                            {item.category}
                          </Option>
                        ))}
                    </Select>
                  )}
                </FormItem>
                <FormItem label="店铺次分类" {...formItemLayout}>
                  {getFieldDecorator("category_id", {
                    rules: [{ required: true, message: "请选择服务商次分类" }],
                    initialValue: this.state.record.category
                  })(
                    <Select
                      placeholder="请选择服务商次分类"
                      style={{ width: 350 }}
                    >
                      {this.state.category &&
                        this.state.category.map(item => (
                          <Option key={+item.id} value={+item.id}>
                            {item.category}
                          </Option>
                        ))}
                    </Select>
                  )}
                </FormItem>
                */}
                {/* 
                <FormItem label="服务商状态" {...formItemLayout}>
                  {getFieldDecorator("status", {
                    rules: [{ required: true, message: "请选择服务商状态" }],
                    initialValue: this.state.record.status
                  })(
                    <Select
                      placeholder="请选择服务商状态"
                      style={{ width: 350 }}
                    >
                      <Option key={0} value={0}>
                        待审核
                      </Option>
                      <Option key={1} value={1}>
                        营业中
                      </Option>
                      <Option key={6} value={6}>
                        审核通过
                      </Option>
                      <Option key={2} value={2}>
                        审核未通过
                      </Option>
                    </Select>
                  )}
                </FormItem> */}
                <FormItem label="预约种类" {...formItemLayout}>
                  {getFieldDecorator("_label", {
                    rules: [{ required: true, message: "请选择预约种类" }],
                    initialValue: this.state.record._label
                  })(
                    <Select placeholder="请选择预约种类" style={{ width: 350 }}>
                      <Option key={0} value={0}>
                        排号
                      </Option>
                      <Option key={1} value={1}>
                        预约
                      </Option>
                      <Option key={2} value={2}>
                        上门服务
                      </Option>
                      <Option key={3} value={3}>
                        预约包厢
                      </Option>
                    </Select>
                  )}
                </FormItem>
                <FormItem label="服务商电话" {...formItemLayout}>
                  {getFieldDecorator("shop_phone", {
                    rules: [{ required: true, message: "请输入服务商电话" }],
                    initialValue: this.state.record.shop_phone
                  })(
                    <Input
                      placeholder="请输入服务商电话"
                      style={{ width: 350 }}
                    />
                  )}
                </FormItem>
                <FormItem label="营业时间" {...formItemLayout}>
                  {getFieldDecorator("shop_time", {
                    rules: [{ required: true, message: "请选择营业时间" }],
                    initialValue: this.state.record.shop_time
                  })(
                    <Input
                      placeholder="请选择营业时间"
                      style={{ width: 350 }}
                    />
                  )}
                </FormItem>
                <FormItem label="封面小图" {...formItemLayout}>
                  {getFieldDecorator("imgeurl", {
                    rules: [{ required: true, message: "请选择封面小图" }],
                    initialValue: this.state.imgeurl
                  })(UploadFile(this.state.imgeurl, "imgeurl", 1))}
                </FormItem>
                <FormItem label="内部轮播图" {...formItemLayout}>
                  {getFieldDecorator("carousel", {
                    rules: [{ required: true, message: "请选择内部轮播图" }],
                    initialValue: this.state.carousel
                  })(UploadFile(this.state.carousel, "carousel", 10))}
                </FormItem>
                <FormItem label="营业执照" {...formItemLayout}>
                  {getFieldDecorator("blicense", {
                    rules: [{ required: true, message: "请选择营业执照" }],
                    initialValue: this.state.blicense
                  })(UploadFile(this.state.blicense, "blicense", 1))}
                </FormItem>
                <FormItem label="执业资质证书" {...formItemLayout}>
                  {getFieldDecorator("shopimg", {
                    rules: [{ required: true, message: "请选择执业资质证书" }],
                    initialValue: this.state.shopimg
                  })(UploadFile(this.state.shopimg, "shopimg", 8))}
                </FormItem>
                <FormItem label="联系人" {...formItemLayout}>
                  {getFieldDecorator("contact_name", {
                    rules: [{ required: true, message: "请选择联系人" }],
                    initialValue: this.state.record.contact_name
                  })(
                    <Input placeholder="请选择联系人" style={{ width: 350 }} />
                  )}
                </FormItem>
                <FormItem label="联系人手机号" {...formItemLayout}>
                  {getFieldDecorator("contact_phone", {
                    rules: [{ required: true, message: "请选择联系人手机号" }],
                    initialValue: this.state.record.contact_phone
                  })(
                    <Input
                      placeholder="请选择联系人手机号"
                      style={{ width: 350 }}
                    />
                  )}
                </FormItem>
                <FormItem label="支付人姓名" {...formItemLayout}>
                  {getFieldDecorator("payname", {
                    rules: [{ required: true, message: "请选择支付人姓名" }],
                    initialValue: this.state.record.payname
                  })(
                    <Input
                      placeholder="请选择支付人姓名"
                      style={{ width: 350 }}
                    />
                  )}
                </FormItem>
                <FormItem label="联系人手机号" {...formItemLayout}>
                  {getFieldDecorator("contact_phone", {
                    rules: [{ required: true, message: "请选择联系人手机号" }],
                    initialValue: this.state.record.contact_phone
                  })(
                    <Input
                      placeholder="请选择联系人手机号"
                      style={{ width: 350 }}
                    />
                  )}
                </FormItem>
                <FormItem label="支付方式" {...formItemLayout}>
                  {getFieldDecorator("paykind", {
                    rules: [{ required: true, message: "请选择支付方式" }],
                    initialValue: this.state.record.paykind
                  })(
                    <Input
                      placeholder="请选择支付方式"
                      style={{ width: 350 }}
                    />
                  )}
                </FormItem>
                <FormItem label="支付账号" {...formItemLayout}>
                  {getFieldDecorator("paycard", {
                    rules: [{ required: true, message: "请选择支付账号" }],
                    initialValue: this.state.record.paycard
                  })(
                    <Input
                      placeholder="请选择支付账号"
                      style={{ width: 350 }}
                    />
                  )}
                </FormItem>
                <Row>
                  <Col style={{ textAlign: "center", paddingTop: 50 }}>
                    <Button type="primary" htmlType="submit">
                      提交
                    </Button>
                    <Button onClick={this.handleReset}>重置</Button>
                  </Col>
                </Row>
              </Form>
            )}
            {this.state.operating === "transfer" && (
              <div>
                <Row style={{ paddingBottom: 10 }}>
                  <Col span={12}>服务商名称：{this.state.name}</Col>
                  <Col span={12}>服务商账户名：{this.state.payname}</Col>
                </Row>
                <Row style={{ paddingBottom: 10 }}>
                  <Col span={12}>开户行：{this.state.paykind}</Col>
                  <Col span={12}>账号：{this.state.paycard}</Col>
                </Row>
                <hr />
                <Table
                  rowKey="id"
                  dataSource={this.state.dataForTransfer}
                  columns={this.columnsForTransfer}
                />
              </div>
            )}
            {this.state.operating === "editPwd" && (
              <Form layout="inline" onSubmit={this.handleSubmit}>
                <FormItem label="新密码">
                  {getFieldDecorator("password", {
                    rules: [{ required: true, message: "请输入新密码" }],
                    initialValue: this.state.record.status
                  })(
                    <Input
                      placeholder="请输入新密码"
                      type="password"
                      style={{ width: 350 }}
                    />
                  )}
                </FormItem>
                <Row>
                  <Col style={{ textAlign: "center", paddingTop: 50 }}>
                    <Button type="primary" htmlType="submit">
                      提交
                    </Button>
                    <Button onClick={this.handleReset}>重置</Button>
                  </Col>
                </Row>
              </Form>
            )}
          </Modal>

          <Modal
            title="手机号验证"
            visible={this.state.yzPhone}
            onOk={this.handleOk.bind(this)}
            onCancel={this.handleCancel.bind(this)}
          >
            <Form>
              <FormItem label="手机号">
                <Row gutter={8}>
                  <Col span={12}>
                    {getFieldDecorator('contact_phone', {
                      initialValue: this.state.loginPhone,
                      // rules: [{ required: true, message: '请输入手机号！' }],
                    })(<Input disabled />)}
                  </Col>
                  <Col span={6}>
                    {
                      this.state.checked == true ?
                        <Button onClick={this.timeTip.bind(this)}>{this.state.time}后再次获取验证码</Button> :
                        <Button onClick={this.getCode.bind(this)}>获取验证码</Button>
                    }
                  </Col>
                </Row>
              </FormItem>
              <FormItem label="验证码">
                {getFieldDecorator('code', {
                  // rules: [
                  //   { required: true, message: '验证码不能为空！' }
                  // ],
                })(
                  <Input type="text" placeholder="请输入验证码" />
                )}
              </FormItem>
            </Form>
          </Modal>
        </TabPane>
      </Tabs>
    );
  }
}
const WrappedNormalCategory = Form.create()(Category);

export default hot(module)(WrappedNormalCategory);
