import React, { Component } from "react";
import {
  Form,
  Input,
  Table,
  Select,
  Tabs,
  Button,
  Pagination,
  Tag,
  Modal,
  Row,
  Col,
  message
} from "antd";
import { hot } from "react-hot-loader";
import {
  categorycreate,
  categorysearchx,
  categoryupdate,
  categorydelete
} from "../../api";

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;

class Category extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      operating: "search",
      imgeurl: [],
      carousel: [],
      shopimg: [],
      blicense: [],
      width: 520,
      record: {},
      clientPage: 1
    };
    // this.clientPage = 1
    this.columnsForCategory = [
      {
        title: "id",
        dataIndex: "id",
        key: "id",
        className: "hidden"
      },
      {
        title: "名称",
        dataIndex: "category",
        key: "category"
      },
      {
        title: "分类等级",
        dataIndex: "parent_id",
        key: "parent_id",
        render: text => {
          return text === 0 ? (
            <Tag color="blue">主分类</Tag>
          ) : (
            <Tag color="red">次分类</Tag>
          );
        }
      },
      {
        title: "操作",
        dataIndex: "operation",
        key: "operation",
        render: (text, record, index) => {
          return (
            <div>
              <Button onClick={() => this.edit(record.id, record)}>编辑</Button>
              <Button onClick={() => this.delete(record.id)} type="danger">
                删除
              </Button>
            </div>
          );
        }
      }
    ];
  }

  componentWillMount() {
    this.init();
    this.getCategory();
  }

  getCategory = () => {
    categorysearchx({
      every: "all",
      parent_id: 0
    }).then(res => {
      this.setState({
        category: res.data
      });
    });
  };
  init = (filter = {}) => {
    const req = {
      clientPage: 1,
      everyPage: 10,
      ...filter
    };
    categorysearchx(req).then(
      res => {
        const paginationForShop = {
          showQuickJumper: true,
          total: res.pager.sumpage,
          defaultPageSize: res.pager.everypage,
          onChange: page => {
            req.clientPage = page;
            categorysearchx(req).then(res => {
              this.setState({
                dataForShop: res.data,
                clientPage: page
              });
              // this.clientPage = page
            });
          }
        };

        this.setState({
          dataForShop: res.data,
          paginationForShop,
          clientPage: res.pager.clientpage
        });
        // this.clientPage = res.pager.clientpage
      },
      () => {
        this.setState({
          dataForShop: [],
          paginationForShop: {}
        });
      }
    );
  };

  /**
   * @description { 表单重置 }
   */
  handleReset = () => {
    this.props.form.resetFields();
  };
  /**
   * @description { 表单提交 }
   * @param e { 事件 }
   */
  handleSubmit(e, operating) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (operating === "main_add") {
          categorycreate({
            category: values.main_category
          }).then(() => {
            this.init({
              clientPage: this.state.clientPage
            });
            this.hideModal();
            this.getCategory();
          });
        } else if (operating === "sec_add") {
          if (!values.parent_id) {
            message.warn("请选择服务商主分类");
            return;
          }
          categorycreate({
            parent_id: values.parent_id,
            category: values.sec_category
          }).then(() => {
            this.init({
              clientPage: this.state.clientPage
            });
            this.hideModal();
          });
        } else if (operating === "edit") {
          categoryupdate({
            category: values.category,
            id: this.state.id
          }).then(() => {
            this.init({
              clientPage: this.state.clientPage
            });
            this.hideModal();
          });
        }
      }
    });
  }

  hideModal = () => {
    this.setState({
      visible: false,
      operating: "search",
      width: 520
    });
  };
  /**
   * @description { 删除 }
   * @param key { id }
   */
  delete(id) {
    categorydelete(id).then(() => {
      this.init({
        clientPage: this.state.clientPage
      });
    });
  }
  /**
   * @description { 编辑 }
   * @param key { id }
   */
  edit = (id, record) => {
    this.setState({
      id,
      visible: true,
      title: "编辑",
      record
    });
  };

  /**
   * @description { 弹窗点击取消事件 }
   */
  handleCancel = () => {
    this.setState({
      previewVisible: false
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab="查看服务商" key="1">
          <Form
            layout="inline"
            onSubmit={e => this.handleSubmit(e, "main_add")}
          >
            <FormItem>
              {getFieldDecorator("main_category", {})(
                <Input addonBefore="主分类" placeholder="请输入主分类" />
              )}
            </FormItem>
            <FormItem>
              <Button type="primary" htmlType="submit">
                添加
              </Button>
              <Button onClick={this.handleReset}>重置</Button>
            </FormItem>
            <hr />
          </Form>
          <Form layout="inline" onSubmit={e => this.handleSubmit(e, "sec_add")}>
            <FormItem>
              {getFieldDecorator("parent_id", {})(
                <Select
                  placeholder="请选择服务商主分类"
                  style={{ width: "250px" }}
                >
                  {this.state.category &&
                    this.state.category.map(item => (
                      <Option key={item.id} value={item.id}>
                        {item.category}
                      </Option>
                    ))}
                </Select>
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("sec_category", {})(
                <Input addonBefore="次分类" placeholder="请输入次分类" />
              )}
            </FormItem>
            <FormItem>
              <Button type="primary" htmlType="submit">
                添加
              </Button>
              <Button onClick={this.handleReset}>重置</Button>
            </FormItem>
            <hr />
          </Form>
          <Table
            pagination={false}
            rowKey="id"
            dataSource={this.state.dataForShop}
            columns={this.columnsForCategory}
          />
          <hr />
          <Pagination
            current={this.state.clientPage}
            style={{ textAlign: "right" }}
            {...this.state.paginationForShop}
          />

          <Modal
            title={this.state.title}
            footer={null}
            width={this.state.width}
            visible={this.state.visible}
            onCancel={this.hideModal}
          >
            {this.state.visible && (
              <Form
                layout="inline"
                onSubmit={e => this.handleSubmit(e, "edit")}
              >
                <FormItem label="名称">
                  {getFieldDecorator("category", {
                    rules: [{ required: true, message: "请输入名称" }],
                    initialValue: this.state.record.category
                  })(<Input placeholder="请输入名称" />)}
                </FormItem>
                <Row>
                  <Col style={{ textAlign: "center", paddingTop: 50 }}>
                    <Button type="primary" htmlType="submit">
                      提交
                    </Button>
                    <Button onClick={this.handleReset}>重置</Button>
                  </Col>
                </Row>
              </Form>
            )}
          </Modal>
        </TabPane>
      </Tabs>
    );
  }
}
const WrappedNormalCategory = Form.create()(Category);

export default hot(module)(WrappedNormalCategory);
