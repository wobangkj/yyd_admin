import React, { Component } from "react";
import { Form, Input, Tabs, Button, DatePicker, Select } from "antd";
import { hot } from "react-hot-loader";
import { reportcount } from "../../api";
import moment from "moment";
import "moment/locale/zh-cn";
moment.locale("zh-cn");
const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;
const { RangePicker } = DatePicker;

class App extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      operating: "search",
      data: {}
    };
  }

  componentWillMount() {
    this.init();
  }

  init = (filter = {}) => {
    const req = {
      ...filter
    };
    reportcount(req).then(
      res => {
        this.setState({
          data: res.data
        });
      },
      () => {
        this.setState({
          data: {}
        });
      }
    );
  };

  /**
   * @description { 表单重置 }
   */
  handleReset = () => {
    this.props.form.resetFields();
  };
  /**
   * @description { 表单提交 }
   * @param e { 事件 }
   */
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let time1 = moment().format("YYYY");
        let time2 = moment().format("YYYY");
        if (values.year) {
          time1 = values.year;
          time2 = values.year;
        }
        if (values.section) {
          let date = values.section.split(" ");
          time1 += "-" + date[0];
          time2 += "-" + date[1];
        } else {
          time1 += "-01-01";
          time2 += "-12-31";
        }
        if (values.date) {
          time1 = values.date[0].format("YYYY-MM-DD");
          time2 = values.date[1].format("YYYY-MM-DD");
        }
        this.init({
          time1,
          time2
        });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab="报表" key="1">
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <FormItem label="年份">
              {getFieldDecorator("year", {
                rules: [{ pattern: /^\d{4}$/, message: "年份格式非法" }]
              })(<Input placeholder="请输入年份" />)}
            </FormItem>
            <FormItem label="月份">
              {getFieldDecorator("section", {})(
                <Select placeholder="请选择月份" style={{ width: "250px" }}>
                  <Option key={0} value="01-01 02-01">
                    一月
                  </Option>
                  <Option key={1} value="02-01 03-01">
                    二月
                  </Option>
                  <Option key={2} value="03-01 04-01">
                    三月
                  </Option>
                  <Option key={3} value="04-01 05-01">
                    四月
                  </Option>
                  <Option key={4} value="05-01 06-01">
                    五月
                  </Option>
                  <Option key={5} value="06-01 07-01">
                    六月
                  </Option>
                  <Option key={6} value="07-01 08-01">
                    七月
                  </Option>
                  <Option key={7} value="08-01 09-01">
                    八月
                  </Option>
                  <Option key={8} value="09-01 10-01">
                    九月
                  </Option>
                  <Option key={9} value="10-01 11-01">
                    十月
                  </Option>
                  <Option key={10} value="11-01 12-01">
                    十一月
                  </Option>
                  <Option key={11} value="12-01 12-31">
                    十二月
                  </Option>
                </Select>
              )}
            </FormItem>
            <FormItem label="日期">
              {getFieldDecorator("date", {})(
                <RangePicker format={"YYYY-MM-DD"} />
              )}
            </FormItem>
            <FormItem>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
              <Button onClick={this.handleReset}>重置</Button>
            </FormItem>
            <hr />
            <h3>整体运营情况</h3>
            <p>成交单数：{this.state.data.succ_num}</p>
            <p>付款总金额：{this.state.data.all_money}</p>
          </Form>
        </TabPane>
      </Tabs>
    );
  }
}

export default hot(module)(Form.create()(App));
