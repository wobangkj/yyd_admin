/* eslint-disable eqeqeq */
/* eslint-disable react/jsx-no-undef */
import React, { Component } from 'react';
import { Tabs, Input, Button, Form, message, Row, Col } from 'antd';
import { editp, getCode, getPhone } from '../../api';

const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 2 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 6},
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 2,
    },
  },
}
class chPhone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nickname: '',
      showCheck: false,//控制显示验证手机号
      checked: false,//控制获取验证码按钮点击状态
      time: 60,//获取验证码按钮初始时间 
      phone: null,//用户手机号
    }
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillMount = () => {
    getPhone({
      account: sessionStorage.getItem('username')
    }).then(res => {
      this.setState({
        phone: res.data.phone
      })
    })
  }

  countdown() {
    let that = this
    let { time } = this.state
    if (time != 0) {
      this.setState({
        time: time - 1
      })
      setTimeout(() => {
        that.countdown()
      }, 1000);
    }
    if (time == 0) {
      this.setState({
        checked: false,
        time: 60
      })
    }
  }

  confirmPhone = (rule, values, callback) => {
    if (!values) {
      callback('请输入手机号！');
    }
    else if (values !== this.props.form.getFieldValue('nwePhone')) {
      callback('与输入手机号不同！');
    } else {
      callback();
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        // const req = {
        //   id: sessionStorage.getItem('uid'),
        //   account: values.account,
        //   password: values.password,
        //   phone: values.phone,
        //   code: values.code
        // }
        // adminupdate(req).then((data) => {
        //   if (data.status == 200) {
        //     message.success("修改成功,正在跳转登录界面...", 0.5, () => this.props.history.push('/login'));
        //   } else {
        //     message.error(data.msg, 0.5);
        //   }
        // })
        const req = {
          id: sessionStorage.getItem('uid'),
          phone: values.nwePhone,
          code: values.code,
        }
        editp(req).then(data => {
          if (data.status == 212) {
            message.success("修改成功", 0.5, () => {
              getPhone({
                account: sessionStorage.getItem('username')
              }).then(res => {
                this.setState({
                  phone: res.data.phone
                })
              })
            });
          } else {
            message.error(data.msg, 0.5);
          }
        })
      }
    });
  }
  getCode() {
    this.countdown()
    this.setState({
      checked: true
    })
    getCode({
      phone: this.state.phone,
      type:'phone'
    }).then(res => {
      if (res.status == 212) {
        message.success('验证码发送成功', 0.5)
      }
    })
  }
  timeTip() {
    message.error(`请${this.state.time}后再次尝试`);
  }
  checkName() {
    const account = this.props.form.getFieldValue('account');
    console.log(account)
    if (account == sessionStorage.getItem('username')) {
      this.setState({
        showCheck: true
      })
      getPhone({ account: account }).then(res => {
        this.setState({
          phone: res.data.phone
        })
      })
    } else {
      this.setState({
        showCheck: false
      })
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const style={
      width:'200px'
    }
    return (

      <Form onSubmit={this.handleSubmit}>
        <FormItem label="手机号" {...formItemLayout}>
          <Row gutter={8}>
            <Col span={12}>
              {getFieldDecorator('phone', {
                initialValue: this.state.phone,
                rules: [{ required: true, message: '请输入手机号！' }],
              })(<Input disabled  style={style}/>)}
            </Col>
            <Col span={12}>
              {
                this.state.checked == true ?
                  <Button onClick={this.timeTip.bind(this)}>{this.state.time}后再次获取验证码</Button> :
                  <Button onClick={this.getCode.bind(this)}>获取验证码</Button>
              }
            </Col>
          </Row>
        </FormItem>
        <FormItem {...formItemLayout}
          label="验证码">
          {getFieldDecorator('code', {
            rules: [
              { required: true, message: '验证码不能为空！' }
            ],
          })(
            <Input type="text" placeholder="请输入验证码" style={style}/>
          )}
        </FormItem>
        <FormItem {...formItemLayout}
          label="新手机号">
          {getFieldDecorator('nwePhone', {
            rules: [
              { required: true, message: '新手机号不能为空！' }
            ],
          })(
            <Input type="text" placeholder="请输入新手机号" style={style}/>
          )}
        </FormItem>
        <FormItem {...formItemLayout}
              label="确认手机号">
              {getFieldDecorator('confirmPhone', {
                rules: [
                  { required: true, validator: this.confirmPhone.bind(this) }
                ],
              })(
                <Input type="text" placeholder="请输入确认手机号" style={style}/>
              )}
            </FormItem>
        <FormItem {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" >
            更新
                    </Button>
          {/* <Button htmlType="reset" >
            重设
                    </Button> */}
        </FormItem>
      </Form>
    )
  }
}
const WrappedNormalchPhone = Form.create()(chPhone);

export default (WrappedNormalchPhone);  