/* eslint-disable eqeqeq */
import React, { Component } from "react";
import { Form, Input, Button, message, Modal, Row, Col } from "antd";
import { hot } from "react-hot-loader";
import { API_ROOT } from "../../axios/config";
import { adminlogin, getCode, setPhone, getPhone } from "../../api";
import "../../style/css/login.css";
const formItemLayout = {
  labelCol: {
    xs: { span: 6 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 18 },
    sm: { span: 18 }
  }
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 18,
      offset: 4
    },
    sm: {
      span: 18,
      offset: 4
    }
  }
};
const FormItem = Form.Item;
class Login extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCaptcha = this.handleCaptcha.bind(this);
    this.state = {
      captchaSrc: API_ROOT + "captcha",
      visible: false,
      time: 60,
      phone: ''
    };
  }

  componentWillMount() {
    // 禁用后退按钮
    window.history.pushState(null, null, document.location.href);
    window.onpopstate = function () {
      window.history.go(1);
    };
  }
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        adminlogin(values).then(res => {
          if (res.status === 200) {
            sessionStorage.setItem("username", values.account);
            sessionStorage.setItem("uid", res.uid);
            sessionStorage.setItem("area", res.area);
            sessionStorage.setItem("role", res.role);
            message.success("登录成功，正在跳转控制台...", 0.5, () =>
              this.props.history.push("/app/shop/view")
            );
          }
          if (res.status == 218) {
            message.warning(res.msg, 0.5, () => {
              getPhone({
                account: this.props.form.getFieldValue('account')
              }).then(res => {
                this.setState({
                  visible: true,
                  phone: res.data.phone
                })
              })

            }
            );
          }
        });
      }
    });
  }
  handleCaptcha() {
    this.setState({
      captchaSrc: API_ROOT + "captcha?" + +new Date()
    });
  }
  //手机验证
  handleOk() {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const req = {
          account: values.account,
          phone: values.phone,
          code: values.code
        }
        setPhone(req).then(res => {
          if (res.status === 212) {
            message.success("验证成功，正在跳转控制台...", 0.5, () =>
              this.props.history.push("/app/shop/view")
            );
            this.setState({
              visible: false
            })
          } else {
            message.error(res.msg, 0.5);
          }
        })
      }
    })

  }
  handleCancel() {
    this.setState({
      visible: false
    })
  }
  //获取验证码
  getCode() {
    const phone = this.props.form.getFieldValue('phone')
    this.countdown()
    this.setState({
      checked: true
    })
    getCode({
      phone: phone,
      type:'login'
    }).then(res => {
      message.success('验证码发送成功', 0.5)
    })
  }
  //验证码倒计时
  countdown() {
    let that = this
    let time = this.state.time
    if (time != 0) {
      this.setState({
        time: time - 1
      })
      setTimeout(() => {
        that.countdown()
      }, 1000);
    }
    if (time == 0) {
      this.setState({
        checked: false,
        time: 60
      })
    }
  }
  timeTip() {
    message.error(`请${this.state.time}后再次尝试`);
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="login-wrapper">
        <div className="login">
          <h1 style={{ textAlign: "center", color: "#fff" }}>
            优宜定后台管理系统
          </h1>
          <Form layout="horizontal" onSubmit={this.handleSubmit}>
            <FormItem label="用户名" {...formItemLayout}>
              {getFieldDecorator("account", {
                rules: [{ required: true, message: "请输入用户名!" }]
              })(<Input size="large" placeholder="请输入用户名" />)}
            </FormItem>
            <FormItem label="密码" {...formItemLayout}>
              {getFieldDecorator("password", {
                rules: [{ required: true, message: "请输入密码!" }]
              })(
                <Input size="large" type="password" placeholder="请输入密码" />
              )}
            </FormItem>
            {/* <FormItem label='验证码'  {...formItemLayout}>
                        <Row>
                            <Col span={14}>
                            {getFieldDecorator('captcha', { 
                                rules: [
                                    { required: true, message: '请输入验证码!' },
                                    { len: 4, message: '验证码格式错误!' }
                                ],
                            })(
                                <Input size='large'  placeholder='请输入验证码'/>
                            )}  
                            </Col>
                            <Col span={8} style={{float:'right'}}>
                                <img onClick={ this.handleCaptcha } src={ this.state.captchaSrc } alt=""/>
                            </Col>
                        </Row>
                    </FormItem> */}
            <FormItem {...tailFormItemLayout}>
              <Button
                size="large"
                type="info"
                htmlType="submit"
                style={{ width: "100%" }}
              >
                登录
              </Button>
            </FormItem>
          </Form>
          <Modal
            title="手机号验证"
            visible={this.state.visible}
            onOk={this.handleOk.bind(this)}
            onCancel={this.handleCancel.bind(this)}
          >
            <Form>
              <FormItem label="手机号" {...formItemLayout}>
                <Row gutter={8}>
                  <Col span={12}>
                    {getFieldDecorator('phone', {
                      initialValue:this.state.phone
                      // rules: [{ required: true, message: '请输入手机号！' }],
                    })(<Input disabled />)}
                  </Col>
                  <Col span={6}>
                    {
                      this.state.checked == true ?
                        <Button onClick={this.timeTip.bind(this)}>{this.state.time}后再次获取验证码</Button> :
                        <Button onClick={this.getCode.bind(this)}>获取验证码</Button>
                    }
                  </Col>
                </Row>
              </FormItem>
              <FormItem {...formItemLayout}
                label="验证码">
                {getFieldDecorator('code', {
                  // rules: [
                  //   { required: true, message:'验证码不能为空！' }
                  // ],
                })(
                  <Input type="text" placeholder="请输入验证码" />
                )}
              </FormItem>
            </Form>
          </Modal>
        </div>
      </div>
    );
  }
}

export default hot(module)(Form.create()(Login));
