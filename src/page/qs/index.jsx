import React, { Component } from 'react';
import { Table, Tabs, Pagination } from 'antd';
import { hot } from 'react-hot-loader'
import { suggestsearch } from '../../api'

const TabPane = Tabs.TabPane;


class Suggest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataForShop: []
        }
        this.columnsForSuggest = [
            {
                title: 'id',
                dataIndex: 'id',
                key: 'id',
                className: 'hidden'
            },
            {
                title: '手机号',
                dataIndex: 'phone',
                key: 'phone',
            },
            {
                title: '邮箱',
                dataIndex: 'email',
                key: 'email',
            },
            {
                title: '内容',
                dataIndex: 'content',
                key: 'content',
            },
        ]

    }

    componentWillMount() {

        this.init()
    }


    init = (filter = {}) => {
        const req = {
            clientPage: 1,
            everyPage: 10,
            ...filter
        }
        suggestsearch(req).then(res => {

            const paginationForShop = {
                showQuickJumper: true,
                total: res.pager.sumpage,
                defaultPageSize: res.pager.everypage,
                onChange: (page) => {
                    req.clientPage = page;
                    suggestsearch(req).then((res) => {
                        this.setState({
                            dataForShop: res.data,
                        });
                        this.clientPage = page
                    });
                }
            }

            this.setState({
                dataForShop: res.data,
                paginationForShop,
            })
        }, () => {
            this.setState({
                dataForShop: [],
                paginationForShop: {}
            })
        })
    }

    render() {
        return (
            <Tabs defaultActiveKey="1">
                <TabPane tab="查看反馈" key="1">
                    <Table pagination={false} rowKey='id' dataSource={this.state.dataForShop} columns={this.columnsForSuggest} />
                    <hr />
                    <Pagination  style={{ textAlign: 'right' }} {...this.state.paginationForShop} />
                </TabPane>
            </Tabs>
        )
    }
}

export default hot(module)(Suggest);