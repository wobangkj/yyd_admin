import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Loadable from "react-loadable";
import Loading from "../components/loading/loading";

const ShopView = Loadable({
  loader: () => import("../page/shop/view"),
  loading: Loading
});
const ShopUser = Loadable({
  loader: () => import("../page/shop/user"),
  loading: Loading
});

const OrderView = Loadable({
  loader: () => import("../page/order/view"),
  loading: Loading
});

const ReportView = Loadable({
  loader: () => import("../page/report/"),
  loading: Loading
});

const Category = Loadable({
  loader: () => import("../page/shop/category"),
  loading: Loading
});

const Setting = Loadable({
  loader: () => import("../page/setting/view"),
  loading: Loading
});
const Qs = Loadable({
  loader: () => import("../page/qs"),
  loading: Loading
});
const Pwd = Loadable({
  loader: () => import("../page/setting/chpwd"),
  loading: Loading
});
const CarouselView = Loadable({
  loader: () => import("../page/carousel/view"),
  loading: Loading
});
const CarouselAdd = Loadable({
  loader: () => import("../page/carousel/add"),
  loading: Loading
});
const Carouseledit = Loadable({
  loader: () => import("../page/carousel/edit"),
  loading: Loading
});
const ChangePhone = Loadable({
  loader: () => import("../page/phone/index"),
  loading: Loading
});
export default class AdminRouter extends Component {
  render() {
    return (
      <Switch>e
        <Route exact path="/app/category" component={Category} />
        <Route exact path="/app/setting" component={Setting} />
        <Route exact path="/app/qs" component={Qs} />
        <Route exact path="/app/pwd" component={Pwd} />
        <Route exact path="/app/report/view" component={ReportView} />
        <Route exact path="/app/shop/view" component={ShopView} />
        <Route exact path="/app/shop/user" component={ShopUser} />
        <Route exact path="/app/order/view" component={OrderView} />
        <Route exact path="/app/carousel/view" component={CarouselView} />
        <Route exact path="/app/carousel/add" component={CarouselAdd} />
        <Route exact path="/app/carousel/edit" component={Carouseledit} />
        <Route exact path="/app/phone" component={ChangePhone} />

        <Route render={() => <Redirect to="/404" />} />
      </Switch>
    );
  }
}
