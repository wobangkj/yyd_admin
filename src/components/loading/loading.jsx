import React, { Component } from 'react';
import { hot } from 'react-hot-loader'
import loading from '../../style/icon/loading.gif'

class Loading extends Component {

    render(){
        return(
            <div style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                margin: 'auto',
                width: '100px',
                height: '100px'}}>
                  <img src={loading} alt='loading' />
            </div>
        )
    }
}

export default hot(module)(Loading);
