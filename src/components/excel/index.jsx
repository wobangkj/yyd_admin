import React from 'react';
import { HotTable } from '@handsontable/react';
import { hot } from 'react-hot-loader'
import { Button , Input, Divider, Select, Icon } from 'antd';
import 'handsontable/languages/zh-CN';
import * as XLSX from 'xlsx';
// import 'handsontable/dist/moment/locale/zh-cn';
const InputGroup = Input.Group;
const Option = Select.Option;

class Excel extends React.Component {
  constructor(props) {
    super(props);
    this.state ={ 
    uploadFileName:''
  };
    this.excel = {
      colHeaders: true,
      rowHeaders: true,
      // mergeCells: true,
      contextMenu: true,
      autoWrapRow: true,
      manualRowResize: true,
      manualColumnResize: true,
      manualRowMove: true,
      manualColumnMove: true,
      fixedRowsTop: 2,
      fixedColumnsLeft: 3,
      columnSorting: true,
      sortIndicator: true,
      autoColumnSize: {
        samplingRatio: 23
      },
      stretchH: 'all',
      language:'zh-CN',
        // margeCells: true,
        // mergeCells: [
        //   // {row: 0, col: 0, rowspan: 1, colspan: 2},
        //   // {row: 1, col: 0, rowspan: 2, colspan: 1},
        //   // {row: 1, col: 1, rowspan: 2, colspan: 1},
        //   // {row: 1, col: 2, rowspan: 1, colspan: 2},
        //   // {row: 1, col: 4, rowspan: 1, colspan: 4},
        // ],
      cell: [
        {row: 0, col: 0, className: "htCenter htMiddle"}, // 设置下标为0,0的单元格样式 水平居中、垂直居中
        // {row: 1, col: 3, className: "htCenter htMiddle"},
        {row: 1, col: 4, className: "htCenter htMiddle"},
      ]
        // colHeaders: [
        //   'ID',
        //   'Country',
        //   'Code',
        //   'Currency',
        //   'Level',
        //   'Units',
        //   'Date',
        //   'Change'
        // ]
    };
      // this.columns= [
      //   {
      //     data: 'id',
      //     type: 'numeric',
      //     width: 20
      //   },
      //   {
      //     data: 'flag',
      //     type: 'text'
      //   },
      //   {
      //     data: 'currencyCode',
      //     type: 'text'
      //   },
      //   {
      //     data: 'currency',
      //     type: 'text'
      //   },
      //   {
      //     data: 'level',
      //     type: 'numeric',
      //     numericFormat: {
      //       pattern: '0.0000'
      //     }
      //   },
      //   {
      //     data: 'units',
      //     type: 'text'
      //   },
      //   {
      //     data: 'asOf',
      //     type: 'date',
      //     dateFormat: 'MM/DD/YYYY'
      //   },
      //   {
      //     data: 'onedChng',
      //     type: 'numeric',
      //     numericFormat: {
      //       pattern: '0.00%'
      //     }
      //   }
      // ]
    this.wopts = { bookType: 'xlsx', bookSST: false, type: 'binary' };
      // const wopts = { bookType: 'csv', bookSST: false, type: 'binary' };//ods格式
      // const wopts = { bookType: 'ods', bookSST: false, type: 'binary' };//ods格式
      // const wopts = { bookType: 'xlsb', bookSST: false, type: 'binary' };//xlsb格式
      // const wopts = { bookType: 'fods', bookSST: false, type: 'binary' };//fods格式
      // const wopts = { bookType: 'biff2', bookSST: false, type: 'binary' };//xls格式
      this.upload = this.upload.bind(this);
      this.selectFile = this.selectFile.bind(this);
      this.analyzeData = this.analyzeData.bind(this);
    this.saveAs = this.saveAs.bind(this);
    this.downloadExl = this.downloadExl.bind(this);
    this.s2ab = this.s2ab.bind(this);
    this.onchange = this.onchange.bind(this);
  }
    selectFile() {
      this.filer.click();
    }
    upload(e) {
      var f = e.target.files[0]
      var reader = new FileReader()
      let _this = this
      reader.onload = function (e) {
        var data = e.target.result
        if (_this.rABS) {
          _this.wb = XLSX.read(btoa(this.fixdata(data)), {  // 手动转化
            type: 'base64'
          })
        } else {
          _this.wb = XLSX.read(data, {
            type: 'binary'
          })
        }
        let json = XLSX.utils.sheet_to_json(_this.wb.Sheets[_this.wb.SheetNames[0]])
        _this.analyzeData(json) // analyzeData: 解析导入数据
      }
      if (this.rABS) {
        reader.readAsArrayBuffer(f)
      } else {
        reader.readAsBinaryString(f)
      }
    }
    analyzeData(data){
      let res = [];
      console.log(data);
      for(var i=0, len = data.length ; i < len; i++){
          var keys = Object.values(data[i]);
          var excelItem = {
            id: '',
            flag: '',
            currencyCode: '',
            currency: '',
            level: '',
            units: '',
            asOf: '',
            onedChng: ''
          }
          excelItem.id = keys[0];
          excelItem.flag = keys[1];
          excelItem.currencyCode = keys[2];
          excelItem.currency = keys[3];
          excelItem.level = keys[4];
          excelItem.units = keys[5];
          excelItem.asOf = keys[6];
          excelItem.onedChng = keys[7];
          res.push(excelItem);
      }
      var oldData = this.state.data;
      res.push(oldData)
      this.setState({
        data: res
      })
      console.log(this.state.data)
    }
  saveAs(obj, fileName) {//当然可以自定义简单的下载文件实现方式 
      var tmpa = document.createElement("a");
      tmpa.download = fileName || "下载";
      tmpa.href = URL.createObjectURL(obj); //绑定a标签
      tmpa.click(); //模拟点击实现下载
      setTimeout(function () { //延时释放
          URL.revokeObjectURL(obj); //用URL.revokeObjectURL()来释放这个object URL
      }, 100);
  }
  downloadExl() {
      const wb = { SheetNames: ['Sheet1'], Sheets: {}, Props: {} };
      wb.Sheets['Sheet1'] = XLSX.utils.json_to_sheet(this.props.data);//通过json_to_sheet转成单页(Sheet)数据
      this.saveAs(new Blob([this.s2ab(XLSX.write(wb, this.wopts))], { type: "application/octet-stream" }), this.props.uploadFileName + '.' + (this.wopts.bookType === "biff2"?"xls":this.wopts.bookType));
  }
  s2ab(s) {
    var buf,i;
      if (typeof ArrayBuffer !== 'undefined') {
        buf = new ArrayBuffer(s.length);
          var view = new Uint8Array(buf);
          for (i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
          return buf;
      } else {
        buf = new Array(s.length);
          for (i = 0; i !== s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
          return buf;
      }
  }
  onchange(data){
    this.setState({
      uploadFileName:data.target.value
    })
    this.props.getFileName(data.target.value)
  }

  render() {
    return (
      <div >
        <Button  onClick={this.downloadExl}>在线Excel导出</Button> 
        <Divider type="vertical" />
        保存文件名：<Input style={{width:'250px'}} onChange={this.onchange} defaultValue={this.props.uploadFileName} type="text" placeholder="请输入文件名"/>
        <Divider type="vertical" />
        <Button  onClick={this.props.save} >保存</Button> 
        <hr/>
        <HotTable 
        className={this.props.excelClass}
        data={this.props.data}
        mergeCells={this.props.mergeCells}
        // columns={this.props.columns} 
         {...this.excel}
       />
       <hr/>
        <div>
        <InputGroup compact>
          <Select placeholder="请选择导入类型" defaultValue="收入">
            <Option value="支出">支出</Option>
            <Option value="收入">收入</Option>
            <Option value="接待消费流水">接待消费流水</Option>
            <Option value="其他">其他</Option>
          </Select>
          <Button onClick={this.selectFile}>
            <Icon type="upload" /> 在线Excel导入
          </Button>
        </InputGroup>
        </div>
        <input style={{display: 'none'}} ref={ ref=>this.filer = ref} onChange={this.upload} type="file" name="file"/>
    
      </div>
    );
  }
}
export default hot(module)(Excel);
