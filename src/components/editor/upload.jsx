import React from 'react';
import '../../style/css/upload.css'
import uploadImg from '../../style/imgs/upload.svg';
import excelImg from '../../style/icon/excel.png';
import fileImg from '../../style/icon/file.png';
import mp3Img from '../../style/icon/mp3.png';
import pdfImg from '../../style/icon/pdf.png';
import photoImg from '../../style/icon/photo.png';
import pptImg from '../../style/icon/ppt.png';
import rarImg from '../../style/icon/rar.png';
import videoImg from '../../style/icon/video.png';
import wordImg from '../../style/icon/word.png';
import zipImg from '../../style/icon/zip.png';
import { message, Progress, Icon } from 'antd';


class Upload extends React.Component{
  constructor(props){
    super(props);
    this.handleUpload = this.handleUpload.bind(this);
    this.handleFIle = this.handleFIle.bind(this);
    this.refFile=null;
    this.state = {
      file: [
      ],
      show:false,
      percent: 0,
      status: 'active'
    }
  }

  handleUpload(e) {
    this.refFile.click();
  }

  content (file){
   var res = '';
   file.map((v,j,arr) =>{
        if(v.path.includes('pdf')){
          res += `<p ><img style='width: 50px;' title='pdf' alt='pdf' src='${pdfImg}'/><a href=${v.path}>${v.name}</a></p>`
        }
        else if(v.path.includes('jpg') || v.path.includes('png') || v.path.includes('gif') || v.path.includes('bmp') || v.path.includes('jpeg')){
          res +=  `<p ><img style='width: 50px;' title='图片' alt='图片' src='${photoImg}'/><a href=${v.path}>${v.name}</a></p>`
        }
        else if(v.path.includes('xls') || v.path.includes('xlsx')){
          res +=  `<p ><img style='width: 50px;' title='excel' alt='excel' src='${excelImg}'/><a href=${v.path}>${v.name}</a></p>`
        }
        else if(v.path.includes('doc') || v.path.includes('docx')){
          res +=  `<p ><img style='width: 50px;' title='word' alt='word' src='${wordImg}'/><a href=${v.path}>${v.name}</a></p>`
        }
        else if(v.path.includes('ppt') || v.path.includes('pptx')){
          res +=  `<p ><img style='width: 50px;' title='ppt' alt='ppt' src='${pptImg}'/><a href=${v.path}>${v.name}</a></p>`
        }
        else if(v.path.includes('mp4') || 
        v.path.includes('m3u8') || 
        v.path.includes('flv') ||
        v.path.includes('swf') ||
        v.path.includes('mkv') ||
        v.path.includes('avi') ||
        v.path.includes('rm') ||
        v.path.includes('rmvb') ||
        v.path.includes('mpeg') ||
        v.path.includes('mpg') ||
        v.path.includes('ogg') ||
        v.path.includes('ogv') ||
        v.path.includes('mov') ||
        v.path.includes('wmv') ||
        v.path.includes('webm') ||
        v.path.includes('wav') ||
        v.path.includes('mid') ||
        v.path.includes('3gp')
      ){
          res +=  `<p ><img style='width: 50px;' title='video' alt='video' src='${videoImg}'/><a href=${v.path}>${v.name}</a></p>`
        }
        else if(v.path.includes('mp3')){
          res +=  `<p ><img style='width: 50px;' title='mp3' alt='mp3' src='${mp3Img}'/><a href=${v.path}>${v.name}</a></p>`
        }
        else if(v.path.includes('zip') ||
        v.path.includes('tar') ||
        v.path.includes('gz') ||
        v.path.includes('7z') ||
        v.path.includes('bz2')
      ){
          res +=  `<p ><img style='width: 50px;' title='zip' alt='zip' src='${zipImg}'/><a href=${v.path}>${v.name}</a></p>`
        }
        else if(v.path.includes('rar')){
          res +=  `<p ><img style='width: 50px;' title='rar' alt='rar' src='${rarImg}'/><a href=${v.path}>${v.name}</a></p>`
        }
        else{
          res +=  `<p ><img style='width: 50px;' title='file' alt='file' src='${fileImg}'/><a href=${v.path}>${v.name}</a></p>`
        }
        return '';
    })
  return res;      
  }

  handleFIle(){
    if(this.refFile.files[0].size/1024/1024 >= 15){
      message.error("文件最大不超过15MB！")
      return false;      
    }
    this.setState({
      show:true,
      percent:10,
      status: 'active'
    });
    var xmlhttp= new XMLHttpRequest();
    var fd = new FormData();
    const _this = this;
    xmlhttp.onreadystatechange=function (res){
      if (xmlhttp.readyState === 4) {
        _this.setState({
          percent:60
        });
        if (xmlhttp.status === 200) {
          _this.setState({
            percent:70
          });
          var data = JSON.parse(res.currentTarget.response);

          if(data.status === 200){
            _this.state.file.push(data.data);
            var allFile=_this.state.file;
            _this.setState({
              file: allFile,
              percent:100
            })
            
            _this.props.getFileData && _this.props.getFileData(_this.content(_this.state.file));
          }
          else {
            message.warn(data.msg)
            _this.setState({
              percent:100,
              status:'exception'
            });
          }
          setTimeout(function(){
            _this.setState({
              show: false,
              percent:0
            })
          },1000);
          _this.props.getFileData && _this.props.getFileData(_this.content(_this.state.file));

        }
        else {
          message.error("上传失败，网络错误！")
          _this.setState({
            percent:100,
            status:'exception'
          });
          setTimeout(function(){
            _this.setState({
              show: false,
              percent:0
            })
          },1000);
          _this.props.getFileData && _this.props.getFileData(_this.content(_this.state.file));

        }
      }
    };
    fd.append(this.props.name, this.refFile.files[0])
    xmlhttp.open("POST",this.props.action,true);
    xmlhttp.withCredentials = true;
    xmlhttp.send(fd)
    this.setState({
      percent:20
    });
  }
  render (){
    return (
      <form encType="multipart/form-data" method="post">
        <div onClick={this.handleUpload} className="upload-action">
          <img alt="附件" style={{width:'50px'}} src={uploadImg}/><span style={{paddingLeft: '8px'}}>上传附件</span>
        </div>
        <input style={{visibility: 'hidden'}} ref={ref => this.refFile=ref} onChange={this.handleFIle} type="file" name={this.props.name}/>
        <Progress status={this.state.status} percent={this.state.percent} style={{display:this.state.show ? 'inline-block':'none'}} />
        <div className="show-fils">
            {
              this.state.file.map((v,i,arr) =>{
                return <p key={v.name}><Icon  type="file" />{v.name}</p>
              })
            }
        </div>
      </form>
    )
  }
}

export default Upload;