// import React from 'react';
// import { message } from 'antd';
// import * as C from '../../axios/config';
// import BraftEditor from 'braft-editor'
// // import 'braft-editor/dist/braft.css'
// import Upload from './upload';
// import '../../style/css/upload.css';
// class Editor extends React.Component {
//   constructor(props) {
//     super(props);
//     this.getFileData= this.getFileData.bind(this);
//     this.getContent= this.getContent.bind(this);
//     this.getHTMLContent= this.getHTMLContent.bind(this);
//     this.state={
//       affixContent:''
//     };
//     this.editorInstance = null;
//     this.affix = {
//     };
//     this.media= {
//       allowPasteImage: true, // 是否允许直接粘贴剪贴板图片（例如QQ截图等）到编辑器
//       image: true, // 开启图片插入功能
//       video: true, // 开启视频插入功能
//       audio: true, // 开启音频插入功能
//       validateFn: () => {
//         return true;
//       }, // 指定本地校验函数，说明见下文
//       uploadFn: (param) => {
//         const serverURL = C.UPLOAD_AFFIX;
//         const xhr = new XMLHttpRequest();
//         const fd = new FormData();
    
//         // libraryId可用于通过mediaLibrary示例来操作对应的媒体内容
//         // console.log(param.libraryId)
      
//         const successFn = (response) => {
//           const res = JSON.parse(xhr.responseText);
//           param.success({
//             url: res.data.path,
//             meta: {
//               controls: true,
//               loop: true,
//               autoPlay: false,
//               // poster: res.data.path
//             }
//           })
//         }
//         const progressFn = (event) => {
//           // 上传进度发生变化时调用param.progress
//           param.progress(event.loaded / event.total * 100)
//         }
      
//         const errorFn = (response) => {
//           // 上传发生错误时调用param.error
//           param.error(
//             message.error('上传失败，系统繁忙！')
//           )
//         }
      
//         xhr.upload.addEventListener("progress", progressFn, false)
//         xhr.addEventListener("load", successFn, false)
//         xhr.addEventListener("error", errorFn, false)
//         xhr.addEventListener("abort", errorFn, false)
      
//         fd.append('file', param.file)
//         xhr.open('POST', serverURL, true)
//         xhr.send(fd)
//       }, // 指定上传函数，说明见下文
//       removeConfirmFn: null, // 指定删除前的确认函数，说明见下文
//       onRemove: null, // 指定媒体库文件被删除时的回调，参数为被删除的媒体文件列表(数组)
//       onChange: null, // 指定媒体库文件列表发生变化时的回调，参数为媒体库文件列表(数组)
//       onInsert: null, // 指定从媒体库插入文件到编辑器时的回调，参数为被插入的媒体文件列表(数组)
//     };
//     this.fontFamilies= [
//       {
//         name: '微软雅黑',
//         family: 'Microsoft YaHei， sans-serif'
//       },{
//         name: '黑体',
//         family: 'SimHei， sans-serif'
//       },{
//         name: '宋体',
//         family: 'SimSun， sans-serif'
//       },{
//         name: '楷体',
//         family: 'DFKai-SB， sans-serif'
//       },{
//         name: '仿宋',
//         family: 'FangSong， sans-serif'
//       },{
//         name: '楷体',
//         family: 'KaiTi， sans-serif'
//       },{
//         name: '华文细黑',
//         family: 'STHeiti Light [STXihei]， sans-serif'
//       },{
//         name: 'Araial',
//         family: 'Arial, Helvetica, sans-serif'
//       }, {
//         name: 'Georgia',
//         family: 'Georgia, serif'
//       }, {
//         name: 'Impact',
//         family: 'Impact, serif'
//       }, {
//         name: 'Monospace',
//         family: '"Courier New", Courier, monospace'
//       }, {
//         name: 'Tahoma',
//         family: "tahoma, arial, 'Hiragino Sans GB', 宋体, sans-serif"
//       }
//     ];
//     this.extendControls= [
//       {
//         type: 'modal',
//         text: '添加附件',
//         html: '添加附件',
//         hoverTitle: '添加附件!',
//         modal: {
//           id: 'affix-modal', // v1.4.0新增，必选
//           title: '添加附件',
//           showClose: true,
//           showCancel: true,
//           showConfirm: true,
//           confirmable: true,
//           onConfirm: () => {
//             this.editorInstance.insertHTML(this.state.affixContent);
//           },
//           children: (
//             <div  style={{width: 480, height: 320, padding: 30}}>
//               <Upload 
//               action={C.UPLOAD_AFFIX}
//               name="file"
//               getFileData={this.getFileData}
              
              
//               />
//             </div >
//           )
//         }
//       }
//     ];

    
//   }
//   getFileData(data){
//     this.setState({
//       affixContent:data
//     })
//   }
//   getContent(){
//     return this.editorInstance.getContent();
//   }
//   getHTMLContent(){
//     return this.editorInstance.getHTMLContent();
//   }
//   render() {
//     return (
//       <BraftEditor 
//       ref={instance => this.editorInstance = instance}
//       media={this.media}
//       extendControls={this.extendControls}
//       fontFamilies={this.fontFamilies}
//       />
//     );
//   }
// }
// export default Editor;